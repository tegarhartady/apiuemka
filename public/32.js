(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[32],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c(
        "section",
        {
          staticClass: "whyme",
          staticStyle: { "background-color": "#fdc134" },
          attrs: { id: "whyme" }
        },
        [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "container" }, [
              _c(
                "div",
                {
                  staticClass: "section-title",
                  attrs: { "data-aos": "fade-up" }
                },
                [
                  _c("h1", { staticStyle: { color: "#213b52" } }, [
                    _c("strong", [_vm._v("Koperasi Simpan Pinjam")])
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/image 1.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/image2.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/image 3.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/image4.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ])
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "section",
        {
          staticClass: "whyme",
          staticStyle: { "background-color": "#213b52" },
          attrs: { id: "whyme" }
        },
        [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "container" }, [
              _c(
                "div",
                {
                  staticClass: "section-title",
                  attrs: { "data-aos": "fade-up" }
                },
                [
                  _c("h1", { staticStyle: { color: "#ffffff" } }, [
                    _c("strong", [_vm._v("Bank Perkreditan Rakyat")])
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/imagebank1.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/imagebank2.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/imagebank3.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/imagebank4.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticStyle: { "list-style": "none" } }, [
                      _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Free biaya provisi")]),
                      _vm._v(" "),
                      _c("li", [_vm._v("Pencairan dana 2 hari")])
                    ]),
                    _vm._v(" "),
                    _c("button", { staticClass: "btn btn-dark" }, [
                      _vm._v("Lihat Detail")
                    ])
                  ])
                ])
              ])
            ])
          ])
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/bprdanksp.vue":
/*!******************************************!*\
  !*** ./resources/js/views/bprdanksp.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bprdanksp.vue?vue&type=template&id=0832b64c& */ "./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/bprdanksp.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./bprdanksp.vue?vue&type=template&id=0832b64c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);