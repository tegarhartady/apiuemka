(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosipage.vue?vue&type=template&id=fa440536&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Promosipage.vue?vue&type=template&id=fa440536& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "artikel-1" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col text-center" }, [
            _c("h1", [_vm._v("Promosi")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mt-5" }, [
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3 img-responsive",
                attrs: { src: "/assets/img/bca.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "/Promosi" }
                  },
                  [_vm._v("Promo cicilan BCA 0%")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3",
                attrs: { src: "/assets/img/adira.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [_vm._v("Corona Ganas Lagi, IHSG Diprediksi Terkoreksi")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mt-5" }, [
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3 img-responsive",
                attrs: { src: "/assets/img/mandiri.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [_vm._v("Promo cicilan BCA 0%")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3",
                attrs: { src: "/assets/img/astrapay.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [_vm._v("Corona Ganas Lagi, IHSG Diprediksi Terkoreksi")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Promosipage.vue":
/*!********************************************!*\
  !*** ./resources/js/views/Promosipage.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Promosipage_vue_vue_type_template_id_fa440536___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Promosipage.vue?vue&type=template&id=fa440536& */ "./resources/js/views/Promosipage.vue?vue&type=template&id=fa440536&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Promosipage_vue_vue_type_template_id_fa440536___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Promosipage_vue_vue_type_template_id_fa440536___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Promosipage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Promosipage.vue?vue&type=template&id=fa440536&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/Promosipage.vue?vue&type=template&id=fa440536& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosipage_vue_vue_type_template_id_fa440536___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Promosipage.vue?vue&type=template&id=fa440536& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosipage.vue?vue&type=template&id=fa440536&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosipage_vue_vue_type_template_id_fa440536___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosipage_vue_vue_type_template_id_fa440536___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);