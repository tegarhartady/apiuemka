(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/login/Register.vue?vue&type=template&id=500ad630&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/login/Register.vue?vue&type=template&id=500ad630& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "main",
      { staticClass: "d-flex align-items-center min-vh-100 py-3 py-md-0" },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "card login-card" }, [
            _c("div", { staticClass: "row no-gutters" }, [
              _c("div", { staticClass: "col-md-6" }, [
                _c("img", {
                  staticClass: "login-card-img",
                  attrs: { src: "assets/img/masuk.png", alt: "login" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6" }, [
                _c("div", { staticClass: "card-body" }, [
                  _c("p", { staticClass: "login-card-description" }, [
                    _vm._v("Buka Akun Uemka")
                  ]),
                  _vm._v(" "),
                  _c("form", { attrs: { action: "#!" } }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c(
                        "label",
                        { staticClass: "sr-only", attrs: { for: "name" } },
                        [_vm._v("Name")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: {
                          type: "name",
                          name: "name",
                          id: "name",
                          placeholder: "Masukan Nama Anda"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        { staticClass: "sr-only", attrs: { for: "email" } },
                        [_vm._v("Email")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: {
                          type: "email",
                          name: "email",
                          id: "email",
                          placeholder: "Masukan Nomor Anda"
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group mb-4" }, [
                      _c(
                        "label",
                        { staticClass: "sr-only", attrs: { for: "email" } },
                        [_vm._v("Email")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: {
                          type: "email",
                          name: "email",
                          id: "email",
                          placeholder: "Masukan Email Anda"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        { staticClass: "sr-only", attrs: { for: "password" } },
                        [_vm._v("Password")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: {
                          type: "password",
                          name: "password",
                          id: "password",
                          placeholder: "*********"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        { staticClass: "sr-only", attrs: { for: "password" } },
                        [_vm._v("Password")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: {
                          type: "password",
                          name: "password",
                          id: "password",
                          placeholder: "*********"
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "btn btn-block login-btn",
                      attrs: {
                        name: "login",
                        id: "login",
                        type: "button",
                        value: "Daftar"
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "button",
                      { staticClass: "btn btn-block register-btn" },
                      [
                        _c("img", {
                          attrs: { src: "Login/assets/images/google.png" }
                        }),
                        _c("span", { staticClass: "ml-4" }, [
                          _vm._v("Sign up with google")
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "login-card-footer-text ml-5" }, [
                    _vm._v(
                      "\n              Sudah punya akun ?\n              "
                    ),
                    _c(
                      "a",
                      { staticClass: "text-reset", attrs: { href: "/masuk" } },
                      [_vm._v("Masuk disini")]
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/login/Register.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/login/Register.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Register_vue_vue_type_template_id_500ad630___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Register.vue?vue&type=template&id=500ad630& */ "./resources/js/views/login/Register.vue?vue&type=template&id=500ad630&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Register_vue_vue_type_template_id_500ad630___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Register_vue_vue_type_template_id_500ad630___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/login/Register.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/login/Register.vue?vue&type=template&id=500ad630&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/login/Register.vue?vue&type=template&id=500ad630& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_500ad630___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=template&id=500ad630& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/login/Register.vue?vue&type=template&id=500ad630&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_500ad630___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_500ad630___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);