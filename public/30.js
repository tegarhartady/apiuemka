(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[30],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Sukses.vue?vue&type=template&id=1821ab57&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Sukses.vue?vue&type=template&id=1821ab57& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "hero" } }, [
      _c(
        "div",
        {
          staticClass: "carousel slide carousel-fade",
          attrs: { id: "heroCarousel", "data-ride": "carousel" }
        },
        [
          _c("div", { staticClass: "portfolio", attrs: { role: "listbox" } }, [
            _c("div", { staticClass: "portfolio container" }, [
              _c(
                "div",
                { staticClass: "row", staticStyle: { "padding-top": "100px" } },
                [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("h1", [
                      _vm._v("Terima kasih "),
                      _c("br"),
                      _vm._v("\n              sudah mendaftar")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-xs-12 col-sm-8" }, [
                        _c("h4", { staticClass: "lead mt-4" }, [
                          _vm._v(
                            "Tim kami akan menghubungi kamu dalam 1 x 24 jam"
                          ),
                          _c("br"),
                          _vm._v(
                            "\n\n                  untuk melakukan verifikasi data kamu "
                          ),
                          _c("br"),
                          _vm._v(
                            "\n                   Pastikan No Handphone dan email kamu aktif."
                          )
                        ]),
                        _vm._v(" "),
                        _c("button", { staticClass: "btn btn-warning mt-3" }, [
                          _vm._v("Kembali ke home")
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6 col-sm-offset-12" }, [
                    _c("img", {
                      attrs: { src: "/assets/img/tegar/Hero/hero.png", alt: "" }
                    })
                  ])
                ]
              )
            ])
          ])
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Sukses.vue":
/*!***************************************!*\
  !*** ./resources/js/views/Sukses.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sukses_vue_vue_type_template_id_1821ab57___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sukses.vue?vue&type=template&id=1821ab57& */ "./resources/js/views/Sukses.vue?vue&type=template&id=1821ab57&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Sukses_vue_vue_type_template_id_1821ab57___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Sukses_vue_vue_type_template_id_1821ab57___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Sukses.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Sukses.vue?vue&type=template&id=1821ab57&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/Sukses.vue?vue&type=template&id=1821ab57& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sukses_vue_vue_type_template_id_1821ab57___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Sukses.vue?vue&type=template&id=1821ab57& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Sukses.vue?vue&type=template&id=1821ab57&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sukses_vue_vue_type_template_id_1821ab57___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sukses_vue_vue_type_template_id_1821ab57___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);