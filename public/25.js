(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[25],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Home.vue?vue&type=template&id=63cd6604& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("main", { attrs: { id: "main" } }, [
      _vm._m(1),
      _vm._v(" "),
      _vm._m(2),
      _vm._v(" "),
      _vm._m(3),
      _vm._v(" "),
      _vm._m(4),
      _vm._v(" "),
      _vm._m(5),
      _vm._v(" "),
      _vm._m(6),
      _vm._v(" "),
      _vm._m(7),
      _vm._v(" "),
      _vm._m(8),
      _vm._v(" "),
      _c("section", { attrs: { id: "kontakwa" } }, [
        _c("div", { staticClass: "container" }, [
          _vm._m(9),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12 justify-content-center" }, [
              _c("img", {
                directives: [
                  { name: "rjs", rawName: "v-rjs", value: 3, expression: "3" }
                ],
                attrs: { src: "assets/img/haha.png", alt: "" }
              })
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "hero" } }, [
      _c(
        "div",
        {
          staticClass: "carousel slide carousel-fade",
          attrs: { id: "heroCarousel", "data-ride": "carousel" }
        },
        [
          _c("div", { staticClass: "home", attrs: { role: "listbox" } }, [
            _c("div", { staticClass: "container" }, [
              _c(
                "div",
                { staticClass: "row", staticStyle: { "padding-top": "100px" } },
                [
                  _c("div", { staticClass: "col-sm-7" }, [
                    _c("h1", [
                      _vm._v(
                        "\n                Pilih Pinjaman\n                "
                      ),
                      _c("br"),
                      _vm._v("\n                Terbaik\n              ")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-xs-12 col-sm-8" }, [
                        _c("h4", { staticClass: "lead mt-4" }, [
                          _vm._v(
                            "\n                    Kami membantu Anda untuk menemukan "
                          ),
                          _c("br"),
                          _vm._v(
                            "\n                    lembaga keuangan terbaik untuk Anda."
                          ),
                          _c("br")
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        { staticClass: "btn btn-warning mt-3 mr-3" },
                        [
                          _vm._v(
                            "\n                  Koperasi Simpan Pinjam\n                "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("button", { staticClass: "btn btn-warning mt-3" }, [
                        _vm._v(
                          "\n                  Bank Perkreditan Rakyat\n                "
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-5 col-sm-offset-12" }, [
                    _c("img", {
                      attrs: {
                        src: "/assets/img/tegar/Hero/hero1.png",
                        alt: ""
                      }
                    })
                  ])
                ]
              )
            ])
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "section-title", attrs: { "data-aos": "fade-up" } },
      [_c("h1", { staticClass: "mt-5" }, [_vm._v("Promo Hari Ini")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "promotion" } }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "col" }, [
          _c(
            "div",
            {
              staticClass: "carousel slide",
              attrs: { id: "carouselExampleControls", "data-ride": "carousel" }
            },
            [
              _c("div", { staticClass: "carousel-inner" }, [
                _c("div", { staticClass: "carousel-item active" }, [
                  _c("img", {
                    staticClass: "d-block w-100",
                    attrs: {
                      src: "/assets/img/Project Cover.png",
                      alt: "First slide"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "carousel-item" }, [
                  _c("img", {
                    staticClass: "d-block w-100",
                    attrs: {
                      src: "/assets/img/Project Cover.png",
                      alt: "Second slide"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "carousel-item" }, [
                  _c("img", {
                    staticClass: "d-block w-100",
                    attrs: {
                      src: "/assets/img/Project Cover.png",
                      alt: "Third slide"
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "carousel-control-prev",
                  attrs: {
                    href: "#carouselExampleControls",
                    role: "button",
                    "data-slide": "prev"
                  }
                },
                [
                  _c("span", {
                    staticClass: "carousel-control-prev-icon",
                    attrs: { "aria-hidden": "true" }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "sr-only" }, [_vm._v("Previous")])
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "carousel-control-next",
                  attrs: {
                    href: "#carouselExampleControls",
                    role: "button",
                    "data-slide": "next"
                  }
                },
                [
                  _c("span", {
                    staticClass: "carousel-control-next-icon",
                    attrs: { "aria-hidden": "true" }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "sr-only" }, [_vm._v("Next")])
                ]
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "text-center" }, [
          _c("button", { staticClass: "btn btn-warning p-2 mt-5" }, [
            _vm._v("\n            Lihat semua promosi\n          ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      { staticClass: "portfolio", attrs: { id: "portfolio" } },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row", attrs: { "data-aos": "fade-up" } }, [
            _c("div", { staticClass: "col-md-7" }, [
              _c("img", {
                staticClass: "img-fluid",
                attrs: { src: "assets/img/infouemka.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-5" }, [
              _c("h1", { staticClass: "mt-5" }, [_vm._v("Siapa Kami ?")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h4", { staticClass: "lead" }, [
                _vm._v(
                  "\n              Uemka selangkah lebih maju dalam memenuhi "
                ),
                _c("br"),
                _vm._v(
                  "\n              kebutuhan utama konsumen di tengah banyaknya "
                ),
                _c("br"),
                _vm._v(
                  "\n              pilihan produk pinjaman, kesederhanaan. "
                ),
                _c("br"),
                _c("br"),
                _vm._v(
                  "\n              Situs ini menyediakan beragam alat bantu untuk "
                ),
                _c("br"),
                _vm._v(
                  "\n              pengguna, di antaranya perbandingan produk, ulasan "
                ),
                _c("br"),
                _vm._v(
                  "\n              konsumen, dan penyematan rating.\n            "
                )
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      { staticClass: "padd-section text-center", attrs: { id: "get-started" } },
      [
        _c(
          "div",
          { staticClass: "container", attrs: { "data-aos": "fade-up" } },
          [
            _c("div", { staticClass: "section-title text-center" }, [
              _c("h2", [_vm._v("Mengapa")]),
              _vm._v(" "),
              _c("img", { attrs: { src: "/images/header.png", alt: "" } })
            ])
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "container mt-5" }, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              {
                staticClass: "col-md-6 col-lg-3",
                attrs: { "data-aos": "zoom-in", "data-aos-delay": "100" }
              },
              [
                _c("div", { staticClass: "feature-block" }, [
                  _c("img", {
                    staticClass: "mb-5",
                    attrs: { src: "assets/img/fitur/fitur1.png", alt: "img" }
                  }),
                  _vm._v(" "),
                  _c("h4", [_vm._v("Helpful")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n                Platform UEMKA membantu UMKM dalam akses permodalan dan\n                membantu Lembaga keuangan dalam mencari data nasabah. Tujuan\n                utama UEMKA adalah membantu memenuhi kebutuhan semua pihak\n              "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "col-md-6 col-lg-3",
                attrs: { "data-aos": "zoom-in", "data-aos-delay": "200" }
              },
              [
                _c("div", { staticClass: "feature-block" }, [
                  _c("img", {
                    staticClass: "mb-5",
                    attrs: { src: "assets/img/fitur/fitur2.png", alt: "img" }
                  }),
                  _vm._v(" "),
                  _c("h4", [_vm._v("Expert")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n                Platform UEMKA memiliki Expert Person di bidang Lembaga\n                keuangan yang mengerti akan kebutuhan Lembaga keuangan dan\n                konsumen. Sehingga platform transform selalu mengikuti\n                perubahan zaman.\n              "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "col-md-6 col-lg-3",
                attrs: { "data-aos": "zoom-in", "data-aos-delay": "300" }
              },
              [
                _c("div", { staticClass: "feature-block" }, [
                  _c("img", {
                    staticClass: "mb-5",
                    attrs: { src: "assets/img/fitur/fitur3.png", alt: "img" }
                  }),
                  _vm._v(" "),
                  _c("h4", [_vm._v("Rapid")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n                Proses kerja yang lebih efektif dan termonitor secara digital\n                sehingga SLS (Service Level Speed) lebih cepat.\n              "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "col-md-6 col-lg-3",
                attrs: { "data-aos": "zoom-in", "data-aos-delay": "300" }
              },
              [
                _c("div", { staticClass: "feature-block" }, [
                  _c("img", {
                    staticClass: "mb-5",
                    attrs: { src: "assets/img/fitur/fitur4.png", alt: "img" }
                  }),
                  _vm._v(" "),
                  _c("h4", [_vm._v("Outstanding")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n                Dengan adanya Platform UEMKA, maka tingkat akurasi kecocokan\n                (%) antara persyaratan Lembaga keuangan dan kondisi UMKM lebih\n                tinggi karena melalui platform UEMKA semua informasi terpapar\n                dengan jelas & transparant.\n              "
                    )
                  ])
                ])
              ]
            )
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "prosesbisnis" } }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c(
            "div",
            { staticClass: "section-title", attrs: { "data-aos": "fade-up" } },
            [_c("h1", [_vm._v("Proses bisnis kami")])]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12 text-center" }, [
          _c("img", { attrs: { src: "assets/img/prosesuemka.png", alt: "" } })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      {
        staticClass: "whyme",
        staticStyle: { "background-color": "#fdc134" },
        attrs: { id: "whyme" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "container" }, [
            _c(
              "div",
              {
                staticClass: "section-title",
                attrs: { "data-aos": "fade-up" }
              },
              [
                _c("h1", { staticStyle: { color: "#213b52" } }, [
                  _c("strong", [_vm._v("Koperasi Simpan Pinjam")])
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body text-center" }, [
                  _c("img", {
                    attrs: { src: "/assets/img/image 1.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("ul", { staticStyle: { "list-style": "none" } }, [
                    _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Free biaya provisi")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Pencairan dana 2 hari")])
                  ]),
                  _vm._v(" "),
                  _c("button", { staticClass: "btn btn-dark" }, [
                    _c(
                      "a",
                      {
                        staticStyle: { color: "yellow" },
                        attrs: { href: "/kspperusahaan" }
                      },
                      [_vm._v("\n                    Lihat Detail")]
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body text-center" }, [
                  _c("img", {
                    attrs: { src: "/assets/img/image2.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("ul", { staticStyle: { "list-style": "none" } }, [
                    _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Free biaya provisi")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Pencairan dana 2 hari")])
                  ]),
                  _vm._v(" "),
                  _c("button", { staticClass: "btn btn-dark" }, [
                    _vm._v("Lihat Detail")
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body text-center" }, [
                  _c("img", {
                    attrs: { src: "/assets/img/image 3.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("ul", { staticStyle: { "list-style": "none" } }, [
                    _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Free biaya provisi")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Pencairan dana 2 hari")])
                  ]),
                  _vm._v(" "),
                  _c("button", { staticClass: "btn btn-dark" }, [
                    _vm._v("Lihat Detail")
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "text-center koperasi" }, [
            _c("button", { staticClass: "btn btn-outline-dark" }, [
              _c("a", { attrs: { href: "/Detailkspdankpr" } }, [
                _vm._v("\n              Lihat lainnya\n              "),
                _c("i", {
                  staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                })
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      {
        staticClass: "whyme",
        staticStyle: { "background-color": "#213b52" },
        attrs: { id: "whyme" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "container" }, [
            _c(
              "div",
              {
                staticClass: "section-title",
                attrs: { "data-aos": "fade-up" }
              },
              [
                _c("h1", { staticStyle: { color: "#ffffff" } }, [
                  _c("strong", [_vm._v("Bank Perkreditan Rakyat")])
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body text-center" }, [
                  _c("img", {
                    attrs: { src: "/assets/img/imagebank1.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("ul", { staticStyle: { "list-style": "none" } }, [
                    _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Free biaya provisi")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Pencairan dana 2 hari")])
                  ]),
                  _vm._v(" "),
                  _c("button", { staticClass: "btn btn-dark" }, [
                    _vm._v("Lihat Detail")
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body text-center" }, [
                  _c("img", {
                    attrs: { src: "/assets/img/imagebank3.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("ul", { staticStyle: { "list-style": "none" } }, [
                    _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Free biaya provisi")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Pencairan dana 2 hari")])
                  ]),
                  _vm._v(" "),
                  _c("button", { staticClass: "btn btn-dark" }, [
                    _vm._v("Lihat Detail")
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body text-center" }, [
                  _c("img", {
                    attrs: { src: "/assets/img/imagebank4.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("ul", { staticStyle: { "list-style": "none" } }, [
                    _c("li", [_vm._v("Pembiayaan mulai dari Rp 10jt")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Tenor peminjaman 5 tahun")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Free biaya provisi")]),
                    _vm._v(" "),
                    _c("li", [_vm._v("Pencairan dana 2 hari")])
                  ]),
                  _vm._v(" "),
                  _c("button", { staticClass: "btn btn-dark" }, [
                    _vm._v("Lihat Detail")
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "text-center koperasi1" }, [
            _c("button", { staticClass: "btn btn-outline-light" }, [
              _vm._v("\n            Lihat lainnya\n            "),
              _c("i", {
                staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
              })
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "comment" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "container-xl" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-sm-12" }, [
              _c("h2", { staticClass: "text-center mb-5" }, [
                _c("b", [_vm._v("Yuk untung bersama!")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-3" }, [
                  _vm._v(
                    "\n                  Lihat apa kata mereka yang telah bergabung Uemka\n                "
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "carousel slide",
                  attrs: { id: "myCarousel", "data-ride": "carousel" }
                },
                [
                  _c("ol", { staticClass: "carousel-indicators" }, [
                    _c("li", {
                      staticClass: "active",
                      attrs: {
                        "data-target": "#myCarousel",
                        "data-slide-to": "0"
                      }
                    }),
                    _vm._v(" "),
                    _c("li", {
                      attrs: {
                        "data-target": "#myCarousel",
                        "data-slide-to": "1"
                      }
                    }),
                    _vm._v(" "),
                    _c("li", {
                      attrs: {
                        "data-target": "#myCarousel",
                        "data-slide-to": "2"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "carousel-inner" }, [
                    _c("div", { staticClass: "carousel-item active" }, [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-sm-6" }, [
                          _c("div", { staticClass: "media" }, [
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: { src: "/assets/img/Picture.png", alt: "" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("div", { staticClass: "testimonial" }, [
                                _c("p", [
                                  _vm._v(
                                    "\n                                Lorem ipsum dolor sit amet, consec adipiscing\n                                elit. Nam eusem scelerisque tempor, varius\n                                quam luctus dui. Mauris magna metus nec.\n                              "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("p", { staticClass: "overview" }, [
                                  _c("b", [_vm._v("Muhammad Aziz")]),
                                  _vm._v(
                                    ", Media Analyst\n                              "
                                  )
                                ])
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-sm-6" }, [
                          _c("div", { staticClass: "media" }, [
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: { src: "/assets/img/Picture.png", alt: "" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("div", { staticClass: "testimonial" }, [
                                _c("p", [
                                  _vm._v(
                                    "\n                                Vestibulum quis quam ut magna consequat\n                                faucibus. Pellentesque eget mi suscipit\n                                tincidunt. Utmtc tempus dictum. Pellentesque\n                                virra.\n                              "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("p", { staticClass: "overview" }, [
                                  _c("b", [_vm._v("Antonio Moreno")]),
                                  _vm._v(
                                    ", Web Developer\n                              "
                                  )
                                ])
                              ])
                            ])
                          ])
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "carousel-item" }, [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-sm-6" }, [
                          _c("div", { staticClass: "media" }, [
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: { src: "/assets/img/Picture.png", alt: "" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("div", { staticClass: "testimonial" }, [
                                _c("p", [
                                  _vm._v(
                                    "\n                                Lorem ipsum dolor sit amet, consec adipiscing\n                                elit. Nam eusem scelerisque tempor, varius\n                                quam luctus dui. Mauris magna metus nec.\n                              "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("p", { staticClass: "overview" }, [
                                  _c("b", [_vm._v("Michael Holz")]),
                                  _vm._v(
                                    ", Seo Analyst\n                              "
                                  )
                                ])
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-sm-6" }, [
                          _c("div", { staticClass: "media" }, [
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: { src: "/assets/img/Picture.png", alt: "" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("div", { staticClass: "testimonial" }, [
                                _c("p", [
                                  _vm._v(
                                    "\n                                Vestibulum quis quam ut magna consequat\n                                faucibus. Pellentesque eget mi suscipit\n                                tincidunt. Utmtc tempus dictum. Pellentesque\n                                virra.\n                              "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("p", { staticClass: "overview" }, [
                                  _c("b", [_vm._v("Mary Saveley")]),
                                  _vm._v(
                                    ", Web Designer\n                              "
                                  )
                                ])
                              ])
                            ])
                          ])
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "carousel-item" }, [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-sm-6" }, [
                          _c("div", { staticClass: "media" }, [
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: { src: "/assets/img/Picture.png", alt: "" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("div", { staticClass: "testimonial" }, [
                                _c("p", [
                                  _vm._v("Bisnis Aku Berkembang Pesat")
                                ]),
                                _vm._v(" "),
                                _c("p", { staticClass: "overview" }, [
                                  _c("b", [_vm._v("Muhammad Aziz")]),
                                  _vm._v(
                                    " , Pedagang Sembako\n                              "
                                  )
                                ])
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-sm-6" }, [
                          _c("div", { staticClass: "media" }, [
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: { src: "/assets/img/Picture.png", alt: "" }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("div", { staticClass: "testimonial" }, [
                                _c("p", [
                                  _vm._v(
                                    "\n                                Vestibulum quis quam ut magna consequat\n                                faucibus. Pellentesque eget mi suscipit\n                                tincidunt. Utmtc tempus dictum. Pellentesque\n                                virra.\n                              "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("p", { staticClass: "overview" }, [
                                  _c("b", [_vm._v("John Williams")]),
                                  _vm._v(
                                    ", Web Developer\n                              "
                                  )
                                ])
                              ])
                            ])
                          ])
                        ])
                      ])
                    ])
                  ])
                ]
              )
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "section-title", attrs: { "data-aos": "fade-up" } },
      [
        _c("h1", { staticStyle: { color: "#213b52" } }, [
          _c("strong", [
            _vm._v("Punya pertanyaan? "),
            _c("br"),
            _vm._v("\n              Hubungi kami")
          ])
        ]),
        _vm._v(" "),
        _c("button", { staticClass: "btn btn-success p-3 mt-3" }, [
          _c("i", { staticClass: "fab fa-whatsapp mr-2" }),
          _vm._v("whatsapp\n          ")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Home.vue":
/*!*************************************!*\
  !*** ./resources/js/views/Home.vue ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=63cd6604& */ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&":
/*!********************************************************************!*\
  !*** ./resources/js/views/Home.vue?vue&type=template&id=63cd6604& ***!
  \********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=63cd6604& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);