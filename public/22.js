(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Artikel.vue?vue&type=template&id=30ecef46&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Artikel.vue?vue&type=template&id=30ecef46& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "artikel-1" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col text-center" }, [
            _c("h1", [_vm._v("Artikel")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mt-5" }, [
          _c("div", { staticClass: "col-lg-4" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3 img-responsive",
                attrs: { src: "/assets/img/atm.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "/Pageartikel" }
                  },
                  [
                    _vm._v(
                      "Masih Dinaungi Sentimen Positif, Begini Pergerakan IHSG\n              Sepeka"
                    )
                  ]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-4" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3",
                attrs: { src: "/assets/img/hp.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "/Pageartikel" }
                  },
                  [_vm._v("Corona Ganas Lagi, IHSG Diprediksi Terkoreksi")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-4" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3",
                attrs: { src: "/assets/img/laptop.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [
                    _vm._v(
                      "Gagal Bayar Rp 1,9 T, Indosterling Mulai Cicil Utang Desember\n              2020"
                    )
                  ]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row mt-5" }, [
          _c("div", { staticClass: "col-lg-4" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3 img-responsive",
                attrs: { src: "/assets/img/emas.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [_vm._v("Dolar AS Melemah Lagi, Pagi Ini di Level Rp 14.140")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-4" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3",
                attrs: { src: "/assets/img/pohon.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [_vm._v("Awal Pekan, IHSG Masih Melaju di Zona Hijau")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-4" }, [
            _c("div", { staticClass: "card" }, [
              _c("img", {
                staticClass: "card-img-top mb-3",
                attrs: { src: "/assets/img/dompet.png", alt: "..." }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "a",
                  {
                    staticClass: "card-text h5 mt-4 font-weight-bold",
                    attrs: { href: "#" }
                  },
                  [
                    _vm._v(
                      "Marak Gagal Bayar Investasi, Siapa Harus Tanggung Jawab?"
                    )
                  ]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v("Senin, 23 Nov 2020 11:44 WIB")
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Artikel.vue":
/*!****************************************!*\
  !*** ./resources/js/views/Artikel.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Artikel_vue_vue_type_template_id_30ecef46___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Artikel.vue?vue&type=template&id=30ecef46& */ "./resources/js/views/Artikel.vue?vue&type=template&id=30ecef46&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Artikel_vue_vue_type_template_id_30ecef46___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Artikel_vue_vue_type_template_id_30ecef46___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Artikel.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Artikel.vue?vue&type=template&id=30ecef46&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/Artikel.vue?vue&type=template&id=30ecef46& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikel_vue_vue_type_template_id_30ecef46___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Artikel.vue?vue&type=template&id=30ecef46& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Artikel.vue?vue&type=template&id=30ecef46&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikel_vue_vue_type_template_id_30ecef46___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikel_vue_vue_type_template_id_30ecef46___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);