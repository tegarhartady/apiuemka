(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Blog.vue?vue&type=template&id=78b5237e&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Blog.vue?vue&type=template&id=78b5237e& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("main", { attrs: { id: "main" } }, [
      _c("section", { staticClass: "blog", attrs: { id: "blog" } }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c(
                "article",
                {
                  staticClass: "entry entry-single",
                  attrs: { "data-aos": "fade-up" }
                },
                [
                  _c("div", { staticClass: "entry-img" }, [
                    _c("img", { attrs: { src: "assets/img/blog-1.jpg" } })
                  ]),
                  _vm._v(" "),
                  _c("h2", { staticClass: "entry-title" }, [
                    _c("a", { attrs: { href: "blog-single.html" } }, [
                      _vm._v("Dolar AS Melemah Lagi")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "entry-meta" }, [
                    _c("ul", [
                      _c("li", { staticClass: "d-flex align-items-center" }, [
                        _c("i", { staticClass: "icofont-user" }),
                        _vm._v(" "),
                        _c("a", { attrs: { href: "blog-single.html" } }, [
                          _vm._v("Ditulis oleh Lilo Pasha")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", { staticClass: "d-flex align-items-center" }, [
                        _c("i", { staticClass: "icofont-wall-clock" }),
                        _vm._v(" "),
                        _c("a", { attrs: { href: "blog-single.html" } }, [
                          _c("time", { attrs: { datetime: "2020-01-01" } }, [
                            _vm._v("Jan 1, 2020")
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", { staticClass: "d-flex align-items-center" }, [
                        _c("i", { staticClass: "icofont-comment" }),
                        _vm._v(" "),
                        _c("a", { attrs: { href: "blog-single.html" } }, [
                          _vm._v(
                            "12\n                                            Comments"
                          )
                        ])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "entry-content" }, [
                    _c("p", [
                      _vm._v(
                        '\n                                  Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC\n"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"\n                                '
                      )
                    ]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "\n                                    Sit repellat hic cupiditate hic ut nemo. Quis nihil sunt non reiciendis. Sequi in\n                                    accusamus harum vel\n                                    aspernatur. Excepturi numquam nihil cumque odio. Et voluptate cupiditate.\n                                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("blockquote", [
                      _c("i", { staticClass: "icofont-quote-left quote-left" }),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "\n                                        Et vero doloremque tempore voluptatem ratione vel aut. Deleniti sunt animi aut.\n                                        Aut eos aliquam\n                                        doloribus minus autem quos.\n                                    "
                        )
                      ]),
                      _vm._v(" "),
                      _c("i", {
                        staticClass: "las la-quote-right quote-right"
                      }),
                      _vm._v(" "),
                      _c("i", {
                        staticClass: "icofont-quote-right quote-right"
                      })
                    ]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "\n                                    Sed quo laboriosam qui architecto. Occaecati repellendus omnis dicta inventore\n                                    tempore provident\n                                    voluptas mollitia aliquid. Id repellendus quia. Asperiores nihil magni dicta est\n                                    suscipit\n                                    perspiciatis. Voluptate ex rerum assumenda dolores nihil quaerat.\n                                    Dolor porro tempora et quibusdam voluptas. Beatae aut at ad qui tempore corrupti\n                                    velit quisquam rerum.\n                                    Omnis dolorum exercitationem harum qui qui blanditiis neque.\n                                    Iusto autem itaque. Repudiandae hic quae aspernatur ea neque qui. Architecto\n                                    voluptatem magni. Vel\n                                    magnam quod et tempora deleniti error rerum nihil tempora.\n                                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("h3", [_vm._v("Et quae iure vel ut odit alias.")]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "\n                                    Officiis animi maxime nulla quo et harum eum quis a. Sit hic in qui quos fugit ut\n                                    rerum atque. Optio\n                                    provident dolores atque voluptatem rem excepturi molestiae qui. Voluptatem laborum\n                                    omnis ullam\n                                    quibusdam perspiciatis nulla nostrum. Voluptatum est libero eum nesciunt aliquid\n                                    qui.\n                                    Quia et suscipit non sequi. Maxime sed odit. Beatae nesciunt nesciunt accusamus quia\n                                    aut ratione\n                                    aspernatur dolor. Sint harum eveniet dicta exercitationem minima. Exercitationem\n                                    omnis asperiores\n                                    natus aperiam dolor consequatur id ex sed. Quibusdam rerum dolores sint consequatur\n                                    quidem ea.\n                                    Beatae minima sunt libero soluta sapiente in rem assumenda. Et qui odit voluptatem.\n                                    Cum quibusdam\n                                    voluptatem voluptatem accusamus mollitia aut atque aut.\n                                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("img", {
                      staticClass: "img-fluid",
                      attrs: { src: "assets/img/blog-5.jpg", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("h3", [
                      _vm._v(
                        "Ut repellat blanditiis est dolore sunt dolorum quae."
                      )
                    ]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "\n                                    Rerum ea est assumenda pariatur quasi et quam. Facilis nam porro amet nostrum. In\n                                    assumenda quia quae\n                                    a id praesentium. Quos deleniti libero sed occaecati aut porro autem. Consectetur\n                                    sed excepturi sint\n                                    non placeat quia repellat incidunt labore. Autem facilis hic dolorum dolores vel.\n                                    Consectetur quasi id et optio praesentium aut asperiores eaque aut. Explicabo omnis\n                                    quibusdam esse. Ex\n                                    libero illum iusto totam et ut aut blanditiis. Veritatis numquam ut illum ut a quam\n                                    vitae.\n                                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "\n                                    Alias quia non aliquid. Eos et ea velit. Voluptatem maxime enim omnis ipsa voluptas\n                                    incidunt. Nulla\n                                    sit eaque mollitia nisi asperiores est veniam.\n                                "
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "entry-footer clearfix" }, [
                    _c("div", { staticClass: "float-left" }, [
                      _c("i", { staticClass: "icofont-folder" }),
                      _vm._v(" "),
                      _c("ul", { staticClass: "cats" }, [
                        _c("li", [
                          _c("a", { attrs: { href: "#" } }, [
                            _vm._v("Business")
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("i", { staticClass: "icofont-tags" }),
                      _vm._v(" "),
                      _c("ul", { staticClass: "tags" }, [
                        _c("li", [
                          _c("a", { attrs: { href: "#" } }, [
                            _vm._v("Creative")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("a", { attrs: { href: "#" } }, [_vm._v("Tips")])
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("a", { attrs: { href: "#" } }, [
                            _vm._v("Marketing")
                          ])
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "float-right share" }, [
                      _c(
                        "a",
                        { attrs: { href: "", title: "Share on Twitter" } },
                        [_c("i", { staticClass: "icofont-twitter" })]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        { attrs: { href: "", title: "Share on Facebook" } },
                        [_c("i", { staticClass: "icofont-facebook" })]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        { attrs: { href: "", title: "Share on Instagram" } },
                        [_c("i", { staticClass: "icofont-instagram" })]
                      )
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "blog-author clearfix",
                  attrs: { "data-aos": "fade-up" }
                },
                [
                  _c("img", {
                    staticClass: "rounded-circle float-left",
                    attrs: { src: "assets/img/blog-author.jpg", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h4", [_vm._v("Jane Smith")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "social-links" }, [
                    _c("a", { attrs: { href: "https://twitters.com/#" } }, [
                      _c("i", { staticClass: "icofont-twitter" })
                    ]),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "https://facebook.com/#" } }, [
                      _c("i", { staticClass: "icofont-facebook" })
                    ]),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "https://instagram.com/#" } }, [
                      _c("i", { staticClass: "icofont-instagram" })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n                                Itaque quidem optio quia voluptatibus dolorem dolor. Modi eum sed possimus accusantium.\n                                Quas repellat\n                                voluptatem officia numquam sint aspernatur voluptas. Esse et accusantium ut unde\n                                voluptas.\n                            "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "blog-comments",
                  attrs: { "data-aos": "fade-up" }
                },
                [
                  _c("h4", { staticClass: "comments-count" }, [
                    _vm._v("8 Comments")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "comment clearfix",
                      attrs: { id: "comment-1" }
                    },
                    [
                      _c("img", {
                        staticClass: "comment-img  float-left",
                        attrs: { src: "assets/img/comments-1.jpg", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("h5", [
                        _c("a", { attrs: { href: "" } }, [
                          _vm._v("Georgia Reader")
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          { staticClass: "reply", attrs: { href: "#" } },
                          [
                            _c("i", { staticClass: "icofont-reply" }),
                            _vm._v(" Reply")
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("time", { attrs: { datetime: "2020-01-01" } }, [
                        _vm._v("01 Jan, 2020")
                      ]),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "\n                                    Et rerum totam nisi. Molestiae vel quam dolorum vel voluptatem et et. Est ad aut\n                                    sapiente quis\n                                    molestiae est qui cum soluta.\n                                    Vero aut rerum vel. Rerum quos laboriosam placeat ex qui. Sint qui facilis et.\n                                "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "comment clearfix",
                      attrs: { id: "comment-2" }
                    },
                    [
                      _c("img", {
                        staticClass: "comment-img  float-left",
                        attrs: { src: "assets/img/comments-2.jpg", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("h5", [
                        _c("a", { attrs: { href: "" } }, [
                          _vm._v("Aron Alvarado")
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          { staticClass: "reply", attrs: { href: "#" } },
                          [
                            _c("i", { staticClass: "icofont-reply" }),
                            _vm._v(
                              "\n                                        Reply"
                            )
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("time", { attrs: { datetime: "2020-01-01" } }, [
                        _vm._v("01 Jan, 2020")
                      ]),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "\n                                    Ipsam tempora sequi voluptatem quis sapiente non. Autem itaque eveniet saepe.\n                                    Officiis illo ut beatae.\n                                "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "comment comment-reply clearfix",
                          attrs: { id: "comment-reply-1" }
                        },
                        [
                          _c("img", {
                            staticClass: "comment-img  float-left",
                            attrs: { src: "assets/img/comments-3.jpg", alt: "" }
                          }),
                          _vm._v(" "),
                          _c("h5", [
                            _c("a", { attrs: { href: "" } }, [
                              _vm._v("Lynda Small")
                            ]),
                            _vm._v(" "),
                            _c(
                              "a",
                              { staticClass: "reply", attrs: { href: "#" } },
                              [
                                _c("i", { staticClass: "icofont-reply" }),
                                _vm._v(" Reply")
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("time", { attrs: { datetime: "2020-01-01" } }, [
                            _vm._v("01 Jan, 2020")
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "\n                                        Enim ipsa eum fugiat fuga repellat. Commodi quo quo dicta. Est ullam aspernatur\n                                        ut vitae quia\n                                        mollitia id non. Qui ad quas nostrum rerum sed necessitatibus aut est. Eum\n                                        officiis sed repellat\n                                        maxime vero nisi natus. Amet nesciunt nesciunt qui illum omnis est et dolor\n                                        recusandae.\n\n                                        Recusandae sit ad aut impedit et. Ipsa labore dolor impedit et natus in porro\n                                        aut. Magnam qui cum.\n                                        Illo similique occaecati nihil modi eligendi. Pariatur distinctio labore omnis\n                                        incidunt et illum.\n                                        Expedita et dignissimos distinctio laborum minima fugiat.\n\n                                        Libero corporis qui. Nam illo odio beatae enim ducimus. Harum reiciendis error\n                                        dolorum non autem\n                                        quisquam vero rerum neque.\n                                    "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "comment comment-reply clearfix",
                              attrs: { id: "comment-reply-2" }
                            },
                            [
                              _c("img", {
                                staticClass: "comment-img  float-left",
                                attrs: {
                                  src: "assets/img/comments-4.jpg",
                                  alt: ""
                                }
                              }),
                              _vm._v(" "),
                              _c("h5", [
                                _c("a", { attrs: { href: "" } }, [
                                  _vm._v("Sianna Ramsay")
                                ]),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "reply",
                                    attrs: { href: "#" }
                                  },
                                  [
                                    _c("i", { staticClass: "icofont-reply" }),
                                    _vm._v(" Reply")
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "time",
                                { attrs: { datetime: "2020-01-01" } },
                                [_vm._v("01 Jan, 2020")]
                              ),
                              _vm._v(" "),
                              _c("p", [
                                _vm._v(
                                  "\n                                            Et dignissimos impedit nulla et quo distinctio ex nemo. Omnis quia dolores\n                                            cupiditate et. Ut unde\n                                            qui eligendi sapiente omnis ullam. Placeat porro est commodi est officiis\n                                            voluptas repellat\n                                            quisquam possimus. Perferendis id consectetur necessitatibus.\n                                        "
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "comment clearfix",
                      attrs: { id: "comment-3" }
                    },
                    [
                      _c("img", {
                        staticClass: "comment-img  float-left",
                        attrs: { src: "assets/img/comments-5.jpg", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("h5", [
                        _c("a", { attrs: { href: "" } }, [
                          _vm._v("Nolan Davidson")
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          { staticClass: "reply", attrs: { href: "#" } },
                          [
                            _c("i", { staticClass: "icofont-reply" }),
                            _vm._v(" Reply")
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("time", { attrs: { datetime: "2020-01-01" } }, [
                        _vm._v("01 Jan, 2020")
                      ]),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "\n                                    Distinctio nesciunt rerum reprehenderit sed. Iste omnis eius repellendus quia nihil\n                                    ut accusantium\n                                    tempore. Nesciunt expedita id dolor exercitationem aspernatur aut quam ut.\n                                    Voluptatem est accusamus\n                                    iste at.\n                                    Non aut et et esse qui sit modi neque. Exercitationem et eos aspernatur. Ea est\n                                    consequuntur officia\n                                    beatae ea aut eos soluta. Non qui dolorum voluptatibus et optio veniam. Quam officia\n                                    sit nostrum\n                                    dolorem.\n                                "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "comment clearfix",
                      attrs: { id: "comment-4" }
                    },
                    [
                      _c("img", {
                        staticClass: "comment-img  float-left",
                        attrs: { src: "assets/img/comments-6.jpg", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("h5", [
                        _c("a", { attrs: { href: "" } }, [
                          _vm._v("Kay Duggan")
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          { staticClass: "reply", attrs: { href: "#" } },
                          [
                            _c("i", { staticClass: "icofont-reply" }),
                            _vm._v(
                              "\n                                        Reply"
                            )
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("time", { attrs: { datetime: "2020-01-01" } }, [
                        _vm._v("01 Jan, 2020")
                      ]),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "\n                                    Dolorem atque aut. Omnis doloremque blanditiis quia eum porro quis ut velit tempore.\n                                    Cumque sed quia\n                                    ut maxime. Est ad aut cum. Ut exercitationem non in fugiat.\n                                "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "reply-form" }, [
                    _c("h4", [_vm._v("Leave a Reply")]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "Your email address will not be published. Required fields are marked * "
                      )
                    ]),
                    _vm._v(" "),
                    _c("form", { attrs: { action: "" } }, [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-md-6 form-group" }, [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: {
                              name: "name",
                              type: "text",
                              placeholder: "Your Name*"
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-6 form-group" }, [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: {
                              name: "email",
                              type: "text",
                              placeholder: "Your Email*"
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col form-group" }, [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: {
                              name: "website",
                              type: "text",
                              placeholder: "Your Website"
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col form-group" }, [
                          _c("textarea", {
                            staticClass: "form-control",
                            attrs: {
                              name: "comment",
                              placeholder: "Your Comment*"
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-primary",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Post Comment")]
                      )
                    ])
                  ])
                ]
              )
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Blog.vue":
/*!*************************************!*\
  !*** ./resources/js/views/Blog.vue ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Blog_vue_vue_type_template_id_78b5237e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Blog.vue?vue&type=template&id=78b5237e& */ "./resources/js/views/Blog.vue?vue&type=template&id=78b5237e&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Blog_vue_vue_type_template_id_78b5237e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Blog_vue_vue_type_template_id_78b5237e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Blog.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Blog.vue?vue&type=template&id=78b5237e&":
/*!********************************************************************!*\
  !*** ./resources/js/views/Blog.vue?vue&type=template&id=78b5237e& ***!
  \********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Blog_vue_vue_type_template_id_78b5237e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Blog.vue?vue&type=template&id=78b5237e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Blog.vue?vue&type=template&id=78b5237e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Blog_vue_vue_type_template_id_78b5237e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Blog_vue_vue_type_template_id_78b5237e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);