(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[28],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosi.vue?vue&type=template&id=46710956&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Promosi.vue?vue&type=template&id=46710956& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("section", { attrs: { id: "headpromosi" } }, [
        _c("div", { staticClass: "row text-center mb-5" }, [
          _c("div", { staticClass: "col" }, [_c("h1", [_vm._v("Promosi")])])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row text-center mb-5" }, [
          _c("div", { staticClass: "col" }, [
            _c("h2", [
              _vm._v("\n          Promo Semangat Akhir Tahun Dana "),
              _c("br"),
              _vm._v("\n          Cair Express dengan BPKB Mobil\n        ")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row text-center mt-5" }, [
          _c("div", { staticClass: "col" }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "assets/img/promosi.png", alt: "" }
            })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { attrs: { id: "syarat" } }, [
        _c("div", { staticClass: "row text-center" }, [
          _c("div", { staticClass: "col" }, [
            _c("h4", [
              _vm._v(
                "\n          Dapatkan dana cair express cukup dengan menjaminkan BPKB mobil\n          "
              ),
              _c("br"),
              _vm._v(
                "\n          Anda! Ajukan pembiayaan dengan jaminan BPKB mobil dan isi form via\n          "
              ),
              _c("br"),
              _vm._v(
                "\n          online untuk mendapatkan cashback special dari BFI Finance!\n        "
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row kotak" }, [
            _c("div", { staticClass: "col" }, [
              _c("h1", [_vm._v("Berikut Syaratnya")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n            Promo hanya berlaku untuk konsumen yang melalukan pengajuan\n            "
                ),
                _c("br"),
                _vm._v(
                  "secara digital di web bfi.co.id dengan pembiayaan BPKB mobil\n            dan berhasil cair, Konsumen berkesempatan mendapatkan bonus hingga\n            Rp 1.000.000 dengan pencairan di atas Rp70.000.000 apabila mengisi\n            form hingga selesai, Satu nomor handphone & identitas resmi/KTP\n            hanya bisa mengikuti promo ini sebanyak satu kali, Pengiriman\n            cashback dilakukan oleh Head Office BFI Finance ke nomor rekening\n            konsumen yang terdaftar dan pengajuannya telah cair\n          "
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row kotak" }, [
            _c("div", { staticClass: "col" }, [
              _c("h1", [_vm._v("Ketentuan umum")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n            Promo akan diberikan langsung oleh BFI Finance kepada pengguna\n            yang pinjamannya disetujui. BFI Finance berhak, tanpa\n            pemberitahuan sebelumnya, melakukan tindakan-tindakan yang\n            diperlukan apabila diduga terjadi tindakan kecurangan dari\n            Peminjam. konsumen setuju untuk menghormati keputusan dan setuju\n            dengan syarat dan ketentuan dari BFI Finance dan Digital Partner.\n            "
                ),
                _c("br"),
                _c("br"),
                _vm._v(
                  "\n            Dapatkan dana cepat dengan BPKB mobil Anda sekarang juga!\n          "
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { attrs: { id: "share" } }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col text-center" }, [
            _c("h4", [_vm._v("Bagikan")]),
            _vm._v(" "),
            _c("img", {
              staticClass: "rounded-circle mr-3",
              attrs: { src: "assets/img/share/m.png", alt: "" }
            }),
            _vm._v(" "),
            _c("img", {
              staticClass: "rounded-circle mr-3",
              attrs: { src: "assets/img/share/c.png", alt: "" }
            }),
            _vm._v(" "),
            _c("img", {
              staticClass: "rounded-circle mr-3",
              attrs: { src: "assets/img/share/iya.png", alt: "" }
            })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { attrs: { id: "ASKME" } }, [
        _c("div", { staticClass: "jumbotron jumbotron-fluid" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-4 col-sm-4" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body" }, [
                    _c("p", { staticClass: "card-text" }, [
                      _vm._v(
                        "\n                  Memiliki pertanyaan seputar pembiayaan?\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("Chat sekarang\n                  "),
                      _c("i", {
                        staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                      })
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-4 col-sm-4" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body" }, [
                    _c("p", { staticClass: "card-text" }, [
                      _vm._v("Ingin mengajukan pembiayaan?")
                    ]),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("AJUKAN SEKARANG\n                  "),
                      _c("i", {
                        staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                      })
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-4 col-sm-4" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body" }, [
                    _c("p", { staticClass: "card-text" }, [
                      _vm._v(
                        "\n                  Temukan cabang atau gerai BFI terdekat\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("LIHAT LOKASI\n                  "),
                      _c("i", {
                        staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                      })
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { attrs: { id: "promotion" } }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col text-center mb-5" }, [
              _c("h1", [_vm._v("Promosi")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col" }, [
            _c(
              "div",
              {
                staticClass: "carousel slide",
                attrs: {
                  id: "carouselExampleControls",
                  "data-ride": "carousel"
                }
              },
              [
                _c("div", { staticClass: "carousel-inner" }, [
                  _c("div", { staticClass: "carousel-item active" }, [
                    _c("img", {
                      staticClass: "d-block w-100",
                      attrs: {
                        src: "/assets/img/Project Cover.png",
                        alt: "First slide"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "carousel-item" }, [
                    _c("img", {
                      staticClass: "d-block w-100",
                      attrs: {
                        src: "/assets/img/Project Cover.png",
                        alt: "Second slide"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "carousel-item" }, [
                    _c("img", {
                      staticClass: "d-block w-100",
                      attrs: {
                        src: "/assets/img/Project Cover.png",
                        alt: "Third slide"
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "carousel-control-prev",
                    attrs: {
                      href: "#carouselExampleControls",
                      role: "button",
                      "data-slide": "prev"
                    }
                  },
                  [
                    _c("span", {
                      staticClass: "carousel-control-prev-icon",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "sr-only" }, [_vm._v("Previous")])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "carousel-control-next",
                    attrs: {
                      href: "#carouselExampleControls",
                      role: "button",
                      "data-slide": "next"
                    }
                  },
                  [
                    _c("span", {
                      staticClass: "carousel-control-next-icon",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "sr-only" }, [_vm._v("Next")])
                  ]
                )
              ]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Promosi.vue":
/*!****************************************!*\
  !*** ./resources/js/views/Promosi.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Promosi.vue?vue&type=template&id=46710956& */ "./resources/js/views/Promosi.vue?vue&type=template&id=46710956&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Promosi.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Promosi.vue?vue&type=template&id=46710956&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/Promosi.vue?vue&type=template&id=46710956& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Promosi.vue?vue&type=template&id=46710956& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosi.vue?vue&type=template&id=46710956&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);