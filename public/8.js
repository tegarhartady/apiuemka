(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Main.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/layouts/Main.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _layouts_Navbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../layouts/Navbar */ "./resources/js/layouts/Navbar.vue");
/* harmony import */ var _layouts_Footer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/Footer */ "./resources/js/layouts/Footer.vue");
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Navbaruemka: _layouts_Navbar__WEBPACK_IMPORTED_MODULE_0__["default"],
    Footeruemka: _layouts_Footer__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Navbar.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/layouts/Navbar.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      partner: [],
      product: [],
      // option: [
      //   axios.get("http://localhost:8000/api/auth/product").then(response => {
      //   this.product = response.data;
      // }),  
      // ]
      rate: [],
      currentValue: ''
    };
  },
  watch: {
    value: {
      handler: function handler(after) {
        this.currentValue = this.format(after);
      },
      immediate: true
    }
  },
  created: function created() {
    this.loadData();
  },
  methods: {
    loadData: function loadData(event) {
      var _this = this;

      axios.get("http://localhost:8000/api/auth/partner").then(function (response) {
        _this.partner = response.data;
      }); // axios.get("http://localhost:8000/api/auth/product/partner/" + event.target.value).then(response => {
      //   this.product = response.data;
      // });
    },
    partnertSelected: function partnertSelected(event) {
      var _this2 = this;

      console.log(event.target.value);
      axios.get("http://localhost:8000/api/auth/product/partner/" + event.target.value).then(function (response) {
        _this2.product = response.data;
      });
    },
    productSelected: function productSelected(event) {
      var _this3 = this;

      console.log(event.target.value);
      axios.get("http://localhost:8000/api/auth/product/rate/" + event.target.value).then(function (response) {
        _this3.rate = response.data;
      });
    },
    format: function format(value) {
      return (value + '').replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },
    handleInput: function handleInput() {
      this.currentValue = this.format(this.currentValue);
      this.$emit('input', (this.currentValue + '').replace(/[^0-9]/g, ""));
    }
  } // data() {
  //   return {
  //     partners:[]
  //   }
  // },
  // methods:{
  //   getPartner : function(){
  //     axios.get('http://127.0.0.1:8000/api/auth/partner')
  //     .then(function (response) {
  //       this.partner = response.data;
  //     }.bind(this))
  //   }
  // }
  // created(){
  //   this.axios
  //   .get('http://localhost:8000/api/auth/partner')
  //   .then(response => {
  //     this.partner = response.data;
  //   })

});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Footer.vue?vue&type=template&id=fbacad02&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/layouts/Footer.vue?vue&type=template&id=fbacad02& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("footer", { attrs: { id: "footer" } }, [
        _c("div", { staticClass: "footer-top" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-3 col-md-6 footer-contact" }, [
                _c("img", {
                  staticClass: "mb-2",
                  attrs: { src: "/images/header.png", alt: "" }
                }),
                _vm._v(" "),
                _c("p", [
                  _vm._v("\n              Uemka bukan lembaga keuangan. "),
                  _c("br"),
                  _vm._v(
                    "\n              Semua materi yang diposting di situs "
                  ),
                  _c("br"),
                  _vm._v(
                    "\n              memiliki karakter informasi. Ketentuan "
                  ),
                  _c("br"),
                  _vm._v("\n              untuk penerbitan layanan produk "),
                  _c("br"),
                  _vm._v(
                    "\n              (pinjaman), silahkan periksa di situs "
                  ),
                  _c("br"),
                  _vm._v(
                    "\n              web lembaga keuangan.\n              "
                  ),
                  _c("br")
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3 col-md-6 footer-links" }, [
                _c("h4", [_vm._v("Layanan")]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [_vm._v("Tentang kami")])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [_vm._v("Produk")])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [_vm._v("Artikel")])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("Daftar Sebagai Lembaga")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3 col-md-6 footer-links" }, [
                _c("h4", [_vm._v("Lokasi Kami")]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _c("p", [
                      _vm._v("\n                  BFI Tower "),
                      _c("br"),
                      _vm._v("\n                  Sunburst CBD Lot. 1.2 "),
                      _c("br"),
                      _vm._v(
                        "\n                  Jl. Kapt. Soebijanto Djojohadikusumo "
                      ),
                      _c("br"),
                      _vm._v(
                        "\n                  BSD City - Tangerang Selatan 15322\n                "
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-3 col-md-6 footer-links" }, [
                _c("h4", [_vm._v("Hubungi Kami")]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _c("img", {
                      staticClass: "mr-3",
                      attrs: { src: "/images/email.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("contact@uemka.com")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("img", {
                      staticClass: "mr-3",
                      attrs: { src: "/images/ig.png", alt: "" }
                    }),
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("@uemka_sitama\n                ")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("img", {
                      staticClass: "mr-3",
                      attrs: { src: "/images/wa.png", alt: "" }
                    }),
                    _c("a", { attrs: { href: "#" } }, [
                      _vm._v("0812 1107 -1420\n                ")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("img", {
                      staticClass: "mr-3",
                      attrs: { src: "/images/fb.png", alt: "" }
                    }),
                    _c("a", { attrs: { href: "#" } }, [_vm._v("Uemka Sitama")])
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "togar container d-md-flex py-4 " }, [
          _c("div", { staticClass: "mr-md-auto text-center text-md-left" }, [
            _c("div", { staticClass: "copyright" }, [
              _vm._v("© 2020 Uemka. All rights reserved")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "text-center text-md-right" }, [
            _c("a", { attrs: { href: "/TermsConditions" } }, [
              _vm._v("Team & Conditions")
            ]),
            _vm._v(" |\n        "),
            _c("a", { attrs: { href: "/Privacy" } }, [
              _vm._v("Privacy Policy")
            ]),
            _vm._v(" |\n        "),
            _c("a", { attrs: { href: "#" } }, [_vm._v("Disclaimer")])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Main.vue?vue&type=template&id=077004c6&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/layouts/Main.vue?vue&type=template&id=077004c6& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Navbaruemka"),
      _vm._v(" "),
      _c("router-view"),
      _vm._v(" "),
      _c("Footeruemka")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Navbar.vue?vue&type=template&id=20dbcd74&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/layouts/Navbar.vue?vue&type=template&id=20dbcd74& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("header", { attrs: { id: "header" } }, [
      _c("div", { staticClass: "container d-flex" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("nav", { staticClass: "nav-menu d-none d-lg-block" }, [
          _c("ul", [
            _vm._m(1),
            _vm._v(" "),
            _vm._m(2),
            _vm._v(" "),
            _vm._m(3),
            _vm._v(" "),
            _vm._m(4),
            _vm._v(" "),
            _vm._m(5),
            _vm._v(" "),
            _c("li", [
              _c(
                "a",
                {
                  staticClass: "pt-1",
                  attrs: {
                    href: "#",
                    "data-toggle": "modal",
                    "data-target": "#exampleModal"
                  }
                },
                [
                  _c(
                    "svg",
                    {
                      attrs: {
                        width: "24",
                        height: "36",
                        viewBox: "0 0 24 36",
                        fill: "none",
                        xmlns: "http://www.w3.org/2000/svg"
                      }
                    },
                    [
                      _c("path", {
                        attrs: {
                          d:
                            "M20.9655 0.758621H3.0345C2.30287 0.758621 1.6012 1.04926 1.08386 1.5666C0.566518 2.08394 0.275879 2.78561 0.275879 3.51724V32.4828C0.275879 33.2144 0.566518 33.9161 1.08386 34.4334C1.6012 34.9507 2.30287 35.2414 3.0345 35.2414H20.9655C21.6972 35.2414 22.3988 34.9507 22.9162 34.4334C23.4335 33.9161 23.7242 33.2144 23.7242 32.4828V3.51724C23.7242 2.78561 23.4335 2.08394 22.9162 1.5666C22.3988 1.04926 21.6972 0.758621 20.9655 0.758621ZM5.10347 21.4483C5.37526 21.4484 5.64437 21.502 5.89544 21.6061C6.1465 21.7102 6.37461 21.8627 6.56673 22.055C6.75885 22.2472 6.91123 22.4754 7.01515 22.7265C7.11908 22.9777 7.17252 23.2468 7.17243 23.5186C7.17234 23.7904 7.11872 24.0595 7.01462 24.3106C6.91053 24.5617 6.75801 24.7898 6.56576 24.9819C6.37351 25.174 6.1453 25.3264 5.89416 25.4303C5.64302 25.5342 5.37388 25.5877 5.10209 25.5876C4.55318 25.5874 4.02683 25.3692 3.63882 24.9809C3.25081 24.5926 3.03294 24.0661 3.03312 23.5172C3.0333 22.9683 3.25153 22.442 3.6398 22.054C4.02806 21.666 4.55456 21.4481 5.10347 21.4483ZM3.0345 16.6207C3.03468 16.0718 3.25291 15.5454 3.64117 15.1574C4.02944 14.7694 4.55594 14.5515 5.10485 14.5517C5.65375 14.5519 6.1801 14.7701 6.56811 15.1584C6.95612 15.5467 7.17399 16.0732 7.17381 16.6221C7.17363 17.171 6.9554 17.6973 6.56713 18.0853C6.17887 18.4733 5.65237 18.6912 5.10347 18.691C4.55456 18.6909 4.02821 18.4726 3.6402 18.0844C3.25219 17.6961 3.03432 17.1696 3.0345 16.6207ZM12 32.4828H5.10347C4.55474 32.4828 4.02849 32.2648 3.64049 31.8768C3.25248 31.4888 3.0345 30.9625 3.0345 30.4138C3.0345 29.8651 3.25248 29.3388 3.64049 28.9508C4.02849 28.5628 4.55474 28.3448 5.10347 28.3448H12C12.5487 28.3448 13.075 28.5628 13.463 28.9508C13.851 29.3388 14.069 29.8651 14.069 30.4138C14.069 30.9625 13.851 31.4888 13.463 31.8768C13.075 32.2648 12.5487 32.4828 12 32.4828ZM12 25.5862C11.7282 25.5861 11.4591 25.5325 11.208 25.4284C10.957 25.3243 10.7289 25.1718 10.5368 24.9795C10.3446 24.7873 10.1923 24.5591 10.0883 24.3079C9.9844 24.0568 9.93096 23.7877 9.93105 23.5159C9.93114 23.2441 9.98477 22.975 10.0889 22.7239C10.193 22.4728 10.3455 22.2447 10.5377 22.0526C10.73 21.8605 10.9582 21.7081 11.2093 21.6042C11.4605 21.5002 11.7296 21.4468 12.0014 21.4469C12.5503 21.4471 13.0767 21.6653 13.4647 22.0536C13.8527 22.4418 14.0705 22.9683 14.0704 23.5172C14.0702 24.0661 13.852 24.5925 13.4637 24.9805C13.0754 25.3685 12.5489 25.5864 12 25.5862ZM12 18.6897C11.4511 18.6895 10.9248 18.4712 10.5368 18.083C10.1487 17.6947 9.93087 17.1682 9.93105 16.6193C9.93123 16.0704 10.1495 15.5441 10.5377 15.156C10.926 14.768 11.4525 14.5502 12.0014 14.5503C12.5503 14.5505 13.0767 14.7688 13.4647 15.157C13.8527 15.5453 14.0705 16.0718 14.0704 16.6207C14.0702 17.1696 13.852 17.6959 13.4637 18.084C13.0754 18.472 12.5489 18.6898 12 18.6897ZM18.8966 32.4828C18.3477 32.4826 17.8213 32.2643 17.4333 31.8761C17.0453 31.4878 16.8274 30.9613 16.8276 30.4124C16.8278 29.8635 17.046 29.3372 17.4343 28.9491C17.8225 28.5611 18.349 28.3433 18.8979 28.3434C19.1697 28.3435 19.4389 28.3972 19.6899 28.5013C19.941 28.6053 20.1691 28.7579 20.3612 28.9501C20.5533 29.1424 20.7057 29.3706 20.8096 29.6217C20.9136 29.8729 20.967 30.142 20.9669 30.4138C20.9668 30.6856 20.9132 30.9547 20.8091 31.2058C20.705 31.4568 20.5525 31.6849 20.3602 31.8771C20.168 32.0692 19.9398 32.2216 19.6886 32.3255C19.4375 32.4294 19.1684 32.4828 18.8966 32.4828ZM18.8966 25.5862C18.6248 25.5861 18.3557 25.5325 18.1046 25.4284C17.8535 25.3243 17.6254 25.1718 17.4333 24.9795C17.2412 24.7873 17.0888 24.5591 16.9849 24.3079C16.881 24.0568 16.8275 23.7877 16.8276 23.5159C16.8277 23.2441 16.8813 22.975 16.9854 22.7239C17.0895 22.4728 17.242 22.2447 17.4343 22.0526C17.6265 21.8605 17.8547 21.7081 18.1059 21.6042C18.357 21.5002 18.6262 21.4468 18.8979 21.4469C19.4469 21.4471 19.9732 21.6653 20.3612 22.0536C20.7492 22.4418 20.9671 22.9683 20.9669 23.5172C20.9667 24.0661 20.7485 24.5925 20.3602 24.9805C19.972 25.3685 19.4455 25.5864 18.8966 25.5862ZM18.8966 18.6897C18.3477 18.6895 17.8213 18.4712 17.4333 18.083C17.0453 17.6947 16.8274 17.1682 16.8276 16.6193C16.8278 16.0704 17.046 15.5441 17.4343 15.156C17.8225 14.768 18.349 14.5502 18.8979 14.5503C19.4469 14.5505 19.9732 14.7688 20.3612 15.157C20.7492 15.5453 20.9671 16.0718 20.9669 16.6207C20.9667 17.1696 20.7485 17.6959 20.3602 18.084C19.972 18.472 19.4455 18.6898 18.8966 18.6897ZM20.9655 9.03448C20.9655 9.76611 20.6749 10.4678 20.1576 10.9851C19.6402 11.5025 18.9385 11.7931 18.2069 11.7931H5.79312C5.06149 11.7931 4.35982 11.5025 3.84248 10.9851C3.32514 10.4678 3.0345 9.76611 3.0345 9.03448V6.27586C3.0345 5.54423 3.32514 4.84256 3.84248 4.32522C4.35982 3.80788 5.06149 3.51724 5.79312 3.51724H18.2069C18.9385 3.51724 19.6402 3.80788 20.1576 4.32522C20.6749 4.84256 20.9655 5.54423 20.9655 6.27586V9.03448Z",
                          fill: "#FDC134"
                        }
                      })
                    ]
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c(
              "li",
              [
                _c("router-link", { attrs: { to: "/masuk" } }, [
                  _c("button", { staticClass: "btn btn-outline ml-3" }, [
                    _vm._v("Masuk")
                  ])
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              [
                _c("router-link", { attrs: { to: "/daftar" } }, [
                  _c("button", { staticClass: "btn btn-warning ml-3" }, [
                    _vm._v("Daftar")
                  ])
                ])
              ],
              1
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "exampleModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "exampleModalLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c("div", { staticClass: "modal-dialog modal-confirm modal-lg" }, [
          _c(
            "div",
            {
              staticClass: "modal-content",
              staticStyle: { position: "absolute", "padding-left": "36px" }
            },
            [
              _vm._m(6),
              _vm._v(" "),
              _vm._m(7),
              _vm._v(" "),
              _c("div", [
                _c(
                  "select",
                  {
                    staticClass: "form-control",
                    attrs: { name: "partner", id: "partner" },
                    on: { change: _vm.partnertSelected }
                  },
                  [
                    _vm._l(_vm.partner, function(part) {
                      return _c(
                        "option",
                        {
                          key: part.idpartner,
                          domProps: { value: part.idpartner }
                        },
                        [
                          _vm._v(
                            "\n              " +
                              _vm._s(part.nama_partner) +
                              "\n            "
                          )
                        ]
                      )
                    }),
                    _vm._v(
                      "\n            Selected  : " +
                        _vm._s(_vm.selected) +
                        "\n          "
                    )
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-5 table" }, [
                  _c("label", { staticClass: "pt-4" }, [
                    _vm._v("Tipe Pinjaman")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.nama_product,
                          expression: "nama_product"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "nameproduct", id: "nameproduct" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.nama_product = $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          },
                          _vm.productSelected
                        ]
                      }
                    },
                    _vm._l(_vm.product, function(pro) {
                      return _c(
                        "option",
                        {
                          key: pro.idproduct,
                          domProps: { value: pro.idproduct }
                        },
                        [
                          _vm._v(
                            "\n                " +
                              _vm._s(pro.nama_product) +
                              "\n              "
                          )
                        ]
                      )
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _c("label", { staticClass: "pt-4" }, [
                    _vm._v("Jumlah pinjaman")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.currentValue,
                        expression: "currentValue"
                      }
                    ],
                    staticClass: "form-control uang",
                    attrs: { type: "text", id: "uang", name: "uang" },
                    domProps: { value: _vm.currentValue },
                    on: {
                      input: [
                        function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.currentValue = $event.target.value
                        },
                        _vm.handleInput
                      ]
                    }
                  }),
                  _vm._v(" "),
                  _c("label", { staticClass: "pt-4" }, [
                    _vm._v("Interest rate (%)")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.interestrate,
                          expression: "interestrate"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "interest", id: "interest" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.interestrate = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    _vm._l(_vm.rate, function(interest) {
                      return _c(
                        "option",
                        {
                          key: interest.idproduct,
                          domProps: { value: interest.idproduct }
                        },
                        [
                          _vm._v(
                            "\n                " +
                              _vm._s(interest.interestrate) +
                              "\n              "
                          )
                        ]
                      )
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _c("label", { staticClass: "pt-4" }, [
                    _vm._v("Jangka waktu pembayaran")
                  ]),
                  _vm._v(" "),
                  _vm._m(8),
                  _vm._v(" "),
                  _vm._m(9)
                ]),
                _vm._v(" "),
                _vm._m(10)
              ])
            ]
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo mr-auto" }, [
      _c("h1", { staticClass: "text-light" }, [
        _c("img", { attrs: { src: "/images/header.png", alt: "" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "active" }, [
      _c("a", { attrs: { href: "portfolio.html" } }, [_vm._v("Tentang Kami")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "drop-down" }, [
      _c("a", { attrs: { href: "" } }, [_vm._v("Pinjaman")]),
      _vm._v(" "),
      _c("ul", [
        _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Drop Down 1")])]),
        _vm._v(" "),
        _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Drop Down 3")])]),
        _vm._v(" "),
        _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Drop Down 4")])]),
        _vm._v(" "),
        _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("Drop Down 5")])])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "/Promosipage" } }, [_vm._v("Promosi")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "/Artikel" } }, [_vm._v("Artikel")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "/Paketpinjol" } }, [
        _vm._v("Daftar Sebagai Lembaga")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header justify-content-center" }, [
      _c(
        "button",
        {
          staticClass: "close",
          staticStyle: { color: "black" },
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-hidden": "true"
          }
        },
        [_vm._v("\n            ×\n          ")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-body text-center" }, [
      _c("h4", [_c("b", [_vm._v("Kalkulator Peminjaman")])])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "select",
      {
        staticClass: "form-control",
        attrs: { name: "jangkawaktu", id: "jangkawaktu" }
      },
      [
        _c("option", { attrs: { disabled: "" } }, [
          _vm._v("Pilih Jangka Waktu ")
        ]),
        _vm._v(" "),
        _c("option", { attrs: { value: "6" } }, [_vm._v("6")]),
        _vm._v(" "),
        _c("option", { attrs: { value: "12" } }, [_vm._v("12")]),
        _vm._v(" "),
        _c("option", { attrs: { value: "18" } }, [_vm._v("18")]),
        _vm._v(" "),
        _c("option", { attrs: { value: "24" } }, [_vm._v("24")]),
        _vm._v(" "),
        _c("option", { attrs: { value: "36" } }, [_vm._v("36")]),
        _vm._v(" "),
        _c("option", { attrs: { value: "48" } }, [_vm._v("48")]),
        _vm._v(" "),
        _c("option", { attrs: { value: "60" } }, [_vm._v("60")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center pt-3 pb-3" }, [
      _c("button", { staticClass: "btn btn-warning" }, [_vm._v("Kalkulasi")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 ml-4 table2" }, [
      _c("div", [
        _c("p", [
          _vm._v("Montly Payment "),
          _c("br"),
          _vm._v("(Principal + Interest)")
        ])
      ]),
      _vm._v(" "),
      _c("div", [
        _c("h1", [_vm._v("Rp 1.000.000")]),
        _vm._v(" "),
        _c("p", [_c("span", [_vm._v("Per Bulan")])]),
        _vm._v(" "),
        _c("button", { staticClass: "btn btn-warning" }, [
          _vm._v("Ajukan Pinjaman")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/layouts/Footer.vue":
/*!*****************************************!*\
  !*** ./resources/js/layouts/Footer.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Footer_vue_vue_type_template_id_fbacad02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Footer.vue?vue&type=template&id=fbacad02& */ "./resources/js/layouts/Footer.vue?vue&type=template&id=fbacad02&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Footer_vue_vue_type_template_id_fbacad02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Footer_vue_vue_type_template_id_fbacad02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/layouts/Footer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/layouts/Footer.vue?vue&type=template&id=fbacad02&":
/*!************************************************************************!*\
  !*** ./resources/js/layouts/Footer.vue?vue&type=template&id=fbacad02& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_fbacad02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=template&id=fbacad02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Footer.vue?vue&type=template&id=fbacad02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_fbacad02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_fbacad02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/layouts/Main.vue":
/*!***************************************!*\
  !*** ./resources/js/layouts/Main.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Main_vue_vue_type_template_id_077004c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Main.vue?vue&type=template&id=077004c6& */ "./resources/js/layouts/Main.vue?vue&type=template&id=077004c6&");
/* harmony import */ var _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Main.vue?vue&type=script&lang=js& */ "./resources/js/layouts/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Main_vue_vue_type_template_id_077004c6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Main_vue_vue_type_template_id_077004c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/layouts/Main.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/layouts/Main.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./resources/js/layouts/Main.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/layouts/Main.vue?vue&type=template&id=077004c6&":
/*!**********************************************************************!*\
  !*** ./resources/js/layouts/Main.vue?vue&type=template&id=077004c6& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_077004c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=template&id=077004c6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Main.vue?vue&type=template&id=077004c6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_077004c6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_077004c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/layouts/Navbar.vue":
/*!*****************************************!*\
  !*** ./resources/js/layouts/Navbar.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar_vue_vue_type_template_id_20dbcd74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar.vue?vue&type=template&id=20dbcd74& */ "./resources/js/layouts/Navbar.vue?vue&type=template&id=20dbcd74&");
/* harmony import */ var _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar.vue?vue&type=script&lang=js& */ "./resources/js/layouts/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Navbar_vue_vue_type_template_id_20dbcd74___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar_vue_vue_type_template_id_20dbcd74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/layouts/Navbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/layouts/Navbar.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/layouts/Navbar.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/layouts/Navbar.vue?vue&type=template&id=20dbcd74&":
/*!************************************************************************!*\
  !*** ./resources/js/layouts/Navbar.vue?vue&type=template&id=20dbcd74& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_20dbcd74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=template&id=20dbcd74& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/Navbar.vue?vue&type=template&id=20dbcd74&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_20dbcd74___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_20dbcd74___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);