<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $table = 'uemka.article';

    protected $fillable = [
        'idarticle','id_login','judul_article','foto_article','updated_at','by_upload','isi_article'
    ];

    protected $primaryKey = 'idarticle';
}
