<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Notifications\Notifiable;
// use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class RegisterLembaga extends Model
{
    // use HasApiTokens,Notifiable;

    protected $table = 'uemka.profile_lembaga';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_perusahaan' ,'nama_perusahaan', 'email_perusahaan', 'alamat_perusahaan', 'website_perusahaan', 'password_perusahaan','nama_lengkap_pic','nomor_hanphone_pic','alamat_pic','created_at'
    ];

    protected $primaryKey = 'id_perusahaan';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    // ];

    // /**
     /* The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
