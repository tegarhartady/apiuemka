<?php

namespace App\Http\Controllers;

use App\Artikel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    public function index()
    {
        return response()->json(Artikel::all(),200);
    }
    
    public function tambah_artikel(Request $request)
    {
        // dd("test");

        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        try {
            // $request->validate([
            //     'judul' => 'required|unique:article',
            //     'foto' => 'required',
            //     'diupload' => 'required',
            //     'isi' => 'required',
            //     // 'publish_at' => 'nullable|date',
            // ]);

            $artikel = new Artikel;
            $artikel->judul_article = $request['judul'];
            $artikel->foto_article = $request['foto'];
            $artikel->by_upload = $request['diupload'];
            $artikel->isi_article = $request['isi'];
            $artikel->updated_at = Carbon::now();
            $artikel->save();
                return response()->json([
                    'message' => 'Artikel Berhasil Di Tambahkan!'
                ], 201);
        } catch (\Throwable $th) {
            //throw $th;
            return $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Article Gagal Di Tambahkan',
                'message' => $th->getMessage(),
            ];
        }
    }

    public function update_artikel(Request $request, $id)
    {
        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        $check_artikel = Artikel::firstWhere('idarticle',$id);
        if (!empty($check_artikel)) {
            try {
                $artikel = Artikel::find($id);
                $artikel->judul_article = $request['judul'];
                $artikel->foto_article = $request['foto'];
                $artikel->by_upload = $request['diupload'];
                $artikel->isi_article = $request['isi'];
                $artikel->updated_at = Carbon::now();
                $artikel->save();
                return response()->json([
                    'message' => 'Artikel Berhasil Di Update!'
                ], 201);
            } catch (\Throwable $th) {
                //throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'Article Gagal Di Update',
                    'message' => $th->getMessage(),
                ];
            }
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Artikel Tidak Ditemukan'
            ], 404);
        }
    }

    public function destroy($id)
    {
        $checkid = Artikel::firstWhere('idarticle',$id);
        if ($checkid) {
            Artikel::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Artikel Berhasil Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Artikel Tidak Di Temukan'
            ],401);
        }
    }

    public function fullarticle(Request $request)
    {
        // dd("test");
        return response()->json(Artikel::all()->where('idarticle','=',$request['idarticle']));
    }
}
