<?php

namespace App\Http\Controllers;

use app\User;
use App\Partner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PartnerController extends Controller
{
    public function data()
    {
        return $this->hasMany('App\User');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Partner::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tambah_partner()
    {
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $register = Partner::all()->where('nohp_partner','=',$request['nohp_partner']);

        foreach ($register as $a) {
            $nohp = $a->nohp_partner;
        }

        if (!empty($nohp)) {
            return $data = [
                'ResponseCode' => '99',
                'ResponseDirection' => 'No Hp Sudah Terdaftar'
            ];
        } else {
            try {
                $partner = new Partner;
                // dd();
                $partner->nama_partner = $request['nama_partner'];
                // $partner->email_partner = $request['email_partner'];
                $partner->alamat_partner = 
                $request['alamat_partner'];
                $partner->nohp_partner = $request['nohp_partner'];
                $partner->website_partner = $request['website_partner'];
                $partner->jenis_partner = $request['jenis_partner'];
                $partner->desc_partner = $request['desc_partner'];
                $partner->visimisi_partner = $request['visimisi_partner'];
                $partner->id_login = $request['id_login'];
                // dd($partner);
                    // 'id_logn'=> Auth::user(),
                $partner->save();
                // Send Mail
                $email = $request['email_partner'];
                $verifikasi = DB::update("UPDATE partner SET verifikasi_partner = 1 WHERE email = '$email'");

                Mail::send('EmailVerification', ['Nama' => $request['nama_partner'], 'Verifikasi' => $verifikasi], 
                function ($message) use ($request) {
                    $message->from('contact@sitama.co.id', 'GrosirMobil');
                    $message->to($request['email']);
                    $message->subject('Verifikasi OTP UEMKA');
                });
                // End Mail
                return response()->json([
                    'message' => 'Partner Berhasil di tambahkan'
                ], 201);
            } catch (\Throwable $th) {
                //throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescreption' => 'Partner Gagal Terdaftar',
                    'Message' => $th->getMessage()
                ];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check_partner = Partner::find($id);
            if ($check_partner) {
                // $request->validate([
                //     'nama_partner' => 'required',
                //     'alamat_partner' => 'required',
                //     'nohp_partner' => 'required',
                //     'website_partner' => 'required',
                //     'jenis_partner' => 'required',
                //     'desc_partner' => 'required',
                //     'visimisi_partner' => 'required',
                //     'id_login' => 'required'
                        // 'created_at' => Carbon::now(),
                        // 'id_login'=> Auth::user(),
                // ]);
                
                $partner = Partner::find($id);
                // $updated = $partner->fill($request->all())-> save();
                $partner->nama_partner = $request->nama_partner;
                $partner->alamat_partner = $request->alamat_partner;
                $partner->nohp_partner = $request->nohp_partner;
                $partner->website_partner = $request->website_partner;
                $partner->jenis_partner = $request->jenis_partner;
                $partner->desc_partner = $request->desc_partner;
                $partner->visimisi_partner = $request->visimisi_partner;
                $partner->id_login = $request->id_login;
                $partner->updated_at = Carbon::now();
                $partner->save();

                return response([
                    'status' => 'OK',
                    'message' => 'Data Partner Berhasil Di Update',
                    'update-data' => $partner
                ],200);
            }else {
                return response([
                    'status' => 'Gagal',
                    'message' => 'Data Partner Tidak di temukan',
                ],401);
            }
        // } catch (\Throwable $th) {
        //     return $data = [
        //         'ResponseCode' => '00',
        //         'ResponseDescription' => 'partner Gagal Di Update',
        //         'message' => $th->getMessage(),
        //     ];
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check_id = Partner::firstWhere('idpartner', $id);
        if ($check_id) {
            Partner::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Partner Sudah Di Hapus'
            ], 200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Partner Tidak Ditemukan'
            ], 404);
        }
    }

    public function update_partner(Request $request, $id)
    {
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        // dd($id);
        // dd($request->nama_partner);
        $updatep = Partner::firstWhere('idpartner',$id);
        if($updatep) {


        //     dd($request);
            $partner = Partner::find($id);
            $partner->nama_partner = $request['nama_partner'];
            $partner->alamat_partner = $request['alamat_partner'];
            $partner->nohp_partner = $request['nohp_partner'];
            $partner->website_partner = $request['website_partner'];
            $partner->jenis_partner = $request['jenis_partner'];
            $partner->desc_partner = $request['desc_partner'];
            $partner->visimisi_partner = $request['visimisi_partner'];
            // $partner->id_login = $request['id_login'];
            $partner->updated_at = Carbon::now();
            $partner->save();
            return response([
                'status' => 'OK',
                'message' => 'Data Partner Berhasil Di Update',
                'update-data' => $partner
            ],200);
        } else {
            return response()->json([
                'Status' => 'Gagal',
                'Message' => 'Data Partner Tidak Berhasil Di Update',
            ], 404);
        }
    }

    public function show_aspartner()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        return response()->json(Partner::all()->where('jenis_partner','=',$request['jenis_partner'],200));
    }
}
