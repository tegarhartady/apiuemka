<?php

namespace App\Http\Controllers\Report;

use App\Partner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportSummaryController extends Controller
{
    public function sumkospin()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);
        $data = DB::select("SELECT count(jenis_partner) as kospin FROM partner WHERE jenis_partner = 'kospin'");
        return response()->json($data);
    }

    public function sumbpr()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);
        $data = DB::select("SELECT count(jenis_partner) as bpr FROM partner WHERE jenis_partner = 'bpr'");
        return response()->json($data);
    }

    public function terdaftar()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $data = DB::select("SELECT count(created_at) as terbaru FROM users ORDER BY created_at DESC");
        return response()->json($data);
    }

    public function getalldata()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $data = DB::table('users')
            ->join('partner','partner.id_login','=','users.id')
            ->join('kalkulator','kalkulator.id_login','=','users.id')
            ->select('users.name','partner.nama_partner','kalkulator.jumlahpinjam','kalkulator.tenor','users.pekerjaan')
            ->get();

        return response()->json($data);
    }
}
