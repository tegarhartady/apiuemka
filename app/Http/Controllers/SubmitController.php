<?php

namespace App\Http\Controllers;

use App\Kalkulator;
use App\Submit;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubmitController extends Controller
{
    public function index()
    {
        return response()->json(Submit::all(),200);
    }

    public function savefromkalku($id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $checkkalid = Kalkulator::firstWhere('idkalkulator',$id);
        if ($checkkalid) {
            try {
                $kalkuid = Kalkulator::find($id);
                $submit = new Submit;
                // $submit->submit_id = $request['submit_id'];
                $submit->kalkulator_id = $checkkalid->id;
                $submit->nama_submit = $request['nama_submit'];
                $submit->email_submit = $request['email_submit'];
                $submit->tenor_submit = $request['tenor_submit'];
                $submit->approve = $request['approve'];
                // $submit->updated_at = Carbon::now();
                // dd($submit);
                $submit->save();
                return response()->json([
                    'message' => 'Berhasil Submit'
                ],201);
            } catch (\Throwable $th) {
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescreption' => 'Submit Gagal Terdaftar',
                    'Message' => $th->getMessage()
                ];
            }   
        } else {
            return response([
                'message' => 'Tidak Berhasil Submit'
            ],401);
        }
    }

    public function editformkalku(Request $request, $id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $checkkalid = Submit::firstWhere('submit_id',$id);
        if ($checkkalid) {
            try {
                $checkid = Submit::find($id);
                // $checkid->submit_id = $request->submit_id;
                // $checkid->kalkulator_id = $request->kalkulator_id;
                $checkid->nama_submit = $request['nama_submit'];
                $checkid->email_submit = $request['email_submit'];
                $checkid->tenor_submit = $request['tenor_submit'];
                $checkid->approve = $request['approve'];
                $checkid->save();
                return response()->json([
                    'message' => 'Berhasil Update Submit'
                ],201); 
            } catch (\Throwable $th) {
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescreption' => 'Submit Gagal Terdaftar',
                    'Message' => $th->getMessage()
                ];
            }
        } else {
            return response([
                'message' => 'Tidak Berhasil Submit'
            ],401);
        }
    }

    public function hapus($id)
    {
        $checkid = Submit::firstWhere('submit_id',$id);

        if ($checkid) {
            Submit::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Submit Berhasil Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Submit tidak Berhasil Di Hapus'
            ],401);
        }
    }
}
