<?php

namespace App\Http\Controllers;

use App\RegisterLembaga;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class LembagaController extends Controller
{
    public function signup(Request $request)
    {

        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        // Where Data Email
        $register = RegisterLembaga::all()->where('email_perusahaan', '=', $request['email_perusahaan']);

        foreach ($register as $a) {
            $email = $a->email_perusahaan;
        }

        if (!empty($email)) {
            return $data=[
                'ResponseCode' => '99',
                'ResponseDirection' => 'Email Sudah Terdaftar'
            ];
        } else {
            try {
                // $request->validate([
                    // 'name' => 'required|string',
                    // 'email_perusahaan' => 'required|string|email|unique:profile_lembaga',
                    // 'website' => 'required|string',
                    // 'namapic' => 'required|string',
                    // 'nohp_pic' => 'required|string',
                    // 'alamatpic' => 'required|string',
                    // 'no_telp' => 'required|numeric',
                    // 'password' => 'required|string|confirmed'                
                // ]);
                $hashpassword = Hash::make($request['password']);
                $lembaga = new RegisterLembaga;
                $lembaga->nama_perusahaan = $request['name_perusahaan'];
                $lembaga->email_perusahaan = $request['email_perusahaan'];
                $lembaga->website_perusahaan = $request['website'];
                $lembaga->nama_lengkap_pic = $request['namapic'];
                $lembaga->nomor_handphone_pic = $request['nohp_pic'];
                $lembaga->alamat_pic = $request['alamatpic'];
                    // 'nomor_handphone_pic' => $request->notelp_pic,
                $lembaga->password_perusahaan = $hashpassword;
                $lembaga->created_at = Carbon::now();
                    // 'level' => '3'                  
                // $lembaga->token_api = Str::random(16);
                // dd($lembaga);
                $lembaga->save();
                $user = new User;
                $user->name = $request['name_perusahaan'];
                $user->email = $request['email_perusahaan'];
                $user->sosmed = $request['website'];
                $user->no_telp = $request['nohp_pic'];
                $user->password = $hashpassword;
                $user->level = '3';
                $user->access_token = Str::random(16);                  
                    // 'token_api' => Str::random(16)
                $user->save();
                // $lembaga->save();
                return response()->json([
                    'message' => 'User Berhasil Terdaftar!'
                ], 201);
            } catch (\Throwable $th) {
                // throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'User Gagal Terdaftar',
                    'message' => $th->getMessage(),
                ];
            }
            
        }
    }

    public function index()
    {
        return response()->json(RegisterLembaga::all(),200);
    }

    public function edit(Request $request, $id)
    {
        $json = file_get_contents('php://input');

        $request = json_decode($json,true);

        $lembagaid = RegisterLembaga::firstWhere('id_perusahaan',$id);

        if ($lembagaid) {
            $daftar = RegisterLembaga::find($id);
            $daftar->nama_perusahaan = $request['nama_perusahaan'];
            $daftar->email_perusahaan = $request['email_perusahaan'];
            $daftar->website_perusahaan = $request['website'];
            $daftar->nama_lengkap_pic = $request['namapic'];
            $daftar->nomor_handphone_pic = $request['nohp_pic'];
            $daftar->alamat_pic = $request['alamatpic'];
            // 'nomor_handphone_pic' => $request->notelp_pic,
            // 'password_perusahaan' => $hashpassword,
            $daftar->updated_at = Carbon::now();
            // 'level' => '3'                  
            // $daftar->token_api = Str::random(16);
            // dd($lembaga);
            $daftar->save();

            return response([
                'status' => 'ok',
                'message' => 'User Berhasil Di Update'
            ],200);
        } else {
            return response()->json([
                'status' => 'Gagal',
                'message' => 'Tidak Berhasil Update'
            ],200);
        }
    }

    public function destroy($id)
    {
        $checkid = RegisterLembaga::firstWhere('id_perusahaan',$id);
        if ($checkid) {
            RegisterLembaga::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Lembaga Berhasil Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Lembaga Tidak Dapat Di Hapus'
            ],401);
        }
    }
    
    public function viewuser(Request $request)
    {
        return response()->json(RegisterLembaga::all()->where('id_perusahaan','=',$request['id_perusahaan']));
    }
}