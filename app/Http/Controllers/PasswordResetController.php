<?php

namespace App\Http\Controllers;

use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(['auth','verified']);
    // }
    public function create(Request $request)
    {
        dd("test");

        $request->validate([
            'email' => 'required|string|email'
        ]);

        $user = User::where('email',$request->email)->first();
        
        if (!$user) 
        {
        return response()->json([
            'email' => "Kami tidak dapat menemukan email tersebut."
        ],  404);

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60)
            ]);
            // dd($passwordReset);
            if ($user && $passwordReset) {
                $user->notify(
                    new PasswordResetRequest($passwordReset->token)
                );

                return response()->json([
                    'Message' => 'Kami mengirim reset password ke email anda'
                ]);
            }
        }
    }

    public function find($token)
    {
        $passwordReset = PasswordReset::where('token',$token)->first();

        if (!$passwordReset) {
            return response()->json([
                'Message' => 'Token reset password ini tidak valid'
            ],404);

            if(Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast())
            {
                $passwordReset->delete();
                return response()->json([
                    'Message' => 'Token reset password ini tidak valid'
                ],404);
            }

            return response()->json($passwordReset);
        }
    }

    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset) 
            return response()->json([
                'Message' => 'Token reset password ini tidak valid'
            ],404);

            $user = User::where('email', $passwordReset->email)->first();
        
        if ($user) {
            return response()->json([
                'message' => 'Kami tidak menemukan user dengan email tersebut'
            ],404);
            $user->password = bcrypt($request->password);
            $user->save();

            $passwordReset->delete();

            $user->notify(new PasswordResetSuccess($passwordReset));

            return response()->json($user);
        }
    }


    public function sendOtpEmail(Request $request)
    {

        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $email = $request['email'];
        $token = $request['tokentype'];

        $user = DB::select("SELECT * FROM apiuemka.users WHERE email = '$email'");

        if (!empty($user)) {
            foreach ($user as $u) {
                $notelpon = $u->notelpon;
                $userid = $u->id;
            }

            $Mobile_Phone = $notelpon;
            $tipeotp = $token;
            $createdat = Carbon::now()->format('Y-m-d');

            try {

                // Send to database
                // $otp = mt_rand(100000, 999999);

                //    send sms gateway
                // $curltoken = curl_init();
                // $data = array('Username' => env('GM_USER', ''), 'password' => env('GM_PASS', ''));
                // curl_setopt($curltoken, CURLOPT_URL, 'https://smsgw.sitama.co.id/api/oauth/token');
                // curl_setopt($curltoken, CURLOPT_POST, 1);
                // curl_setopt($curltoken, CURLOPT_POSTFIELDS, $data);
                // curl_setopt($curltoken, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($curltoken, CURLOPT_SSL_VERIFYPEER, false);
                // $result = curl_exec($curltoken);
                // curl_close($curltoken);

                // $token = json_decode($result, true)['access_token'];

                // // dd($token);

                // $curl = curl_init();

                // curl_setopt_array($curl, array(
                //     CURLOPT_URL => "https://smsgw.sitama.co.id/api/SMS/smssitama",
                //     CURLOPT_RETURNTRANSFER => true,
                //     CURLOPT_SSL_VERIFYPEER => false,
                //     CURLOPT_POSTFIELDS => "{\n    \"notelp\": \"$Mobile_Phone\",\n    \"textsms\": \"Grosir Mobil.id%0AYour OTP Code is $otp\",\n    \"desc\": \"Grosir&Mobil\"\n}",
                //     CURLOPT_HTTPHEADER => array(
                //         "Authorization: Bearer " . $token,
                //         "Content-Type: application/json"
                //     ),
                // ));

                // $response = curl_exec($curl);

                // curl_close($curl);

                // DB::insert("INSERT INTO `e_grosir_admin`.`otplog` (`user_id`, `otp`, `tipeotp`, `created_at`) VALUES ('$userid', '$otp', '$tipeotp', '$createdat');");

                // $data = [
                //     'ResponseCode' => '00',
                //     'ResponseDescription' => 'OTP success to send'
                // ];
            } catch (\Throwable $e) {
                // $data = [
                //     'ResponseCode' => '00',
                //     'ResponseDescription' => 'OTP failed to send'
                // ];
            }
        } else {
            $data = [
                'ResponseCode' => '99',
                'ResponseDescription' => 'Email not found'
            ];
        }

        // Return response
        return response()->json($data, 200);
    }

    public function emailCheck()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $email = $request['email'];

        $users = DB::select("SELECT * FROM apiuemka.users WHERE email = '$email'");

        foreach ($users as $u) {
            $user = $u;
        }

        if (!empty($user)) {
            try {
                $user = User::whereEmail($email)->first();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                // if ($request->remember_me)
                //     $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                $data = [
                    'ResponseCode' => '00',
                    'Users' => $user,
                    'access_token' => $tokenResult->accessToken,
                    'ResponseDescription' => 'Email ditemukan'
                ];
            } catch (\Throwable $e) {
                return $e;
            }
        } else {
            $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Email tidak ditemukan'
            ];
        }

        // Return response
        return response()->json($data, 200);
    }

    public function verificationCode()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        // get request
        $id = $request['userid'];
        $verificationcode = $request['verificationcode'];

        if ($request) {
            // Cek Login
            $cekLoginEmail = DB::select("SELECT * FROM apiuemka.logotp WHERE user_id = '$id'");
            if (!empty($cekLoginEmail)) {
                // cek token in database
                $cekVerificationCode = DB::select("SELECT * FROM  apiuemka.logotp WHERE user_id = '$id' ORDER BY id DESC LIMIT 1");
                if (!empty($cekVerificationCode)) {
                    foreach ($cekVerificationCode as $verif) {
                        // Validasi token
                        if ($verif->otp === $verificationcode) {
                            $data = [
                                'ResponseCode' => '00',
                                'ResponseDescription' => 'Verification Success'
                            ];
                        } else {
                            $data = [
                                'ResponseCode' => '99',
                                'ResponseDescription' => 'Verification Failed'
                            ];
                        }
                    }
                } else {
                    $data = [
                        'ResponseCode' => '99',
                        'ResponseDescription' => 'Verification Failed'
                    ];
                }
            } else {
                $data = [
                    'ResponseCode' => '99',
                    'ResponseDescription' => 'Email not Found'
                ];
            }
        }
        // return data
        return response()->json($data, 200);
    }

    public function changeForgotPassword()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        if ($request) {

            // get request
            $id = $request['userid'];
            $newpassword = $request['newpassword'];
            $confirmpassword = $request['confirmpassword'];

            // Cek LoginID in Database
            $cekLoginID = DB::select("SELECT * FROM e_grosir_admin.users WHERE id = '$id'");

            // Jika Cek Login tidak kosong
            if (!empty($cekLoginID)) {
                // Jika Password Sama
                if ($newpassword === $confirmpassword) {
                    // Action Hash Password
                    $hashnewpassword = Hash::make($newpassword);
                    try {
                        // Update Password yang terhash
                        DB::update("UPDATE `e_grosir_admin`.`users` SET `password` = '$hashnewpassword' WHERE (`id` = '$id');");
                        // Return hasil
                        return $data = [
                            'ResponseCode' => '00',
                            'ResponseDescription' => 'Berhasil Mengubah Password'
                        ];
                    } catch (\Throwable $th) {
                        // Return Hasil
                        return $th;
                    }
                } else {
                    // Return Hasil
                    $data = [
                        'ResponseCode' => '99',
                        'ResponseDescription' => 'Password Not Same'
                    ];
                }
            }
        } else {
            $data = [
                'ResponseCode' => '99',
                'ResponseDescription' => 'Request Not Found'
            ];
        }

        // return response json
        return response()->json($data, 200);
    }
    // End Forgot Password
    public function changePassword()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        if ($request) {

            // get request
            $id = $request['id'];
            $oldpassword = $request['oldpassword'];
            $newpassword = $request['newpassword'];
            $confirmpassword = $request['confirmpassword'];

            // Cek LoginID in Database
            $cekLoginID = DB::select("SELECT * FROM e_grosir_admin.users WHERE id = '$id'");

            // Jika Cek Login tidak kosong
            if (!empty($cekLoginID)) {
                foreach ($cekLoginID as $old) {
                    if (password_verify($oldpassword, $old->password)) {
                        // Jika Password Sama
                        if ($newpassword === $confirmpassword) {
                            // Action Hash Password
                            $hashnewpassword = Hash::make($newpassword);
                            try {
                                // Update Password yang terhash
                                DB::update("UPDATE `e_grosir_admin`.`users` SET `password` = '$hashnewpassword' WHERE (`id` = '$id');");
                                // DB::connection('mysql3')->update("")
                                // Return hasil
                                return $data = [
                                    'ResponseCode' => '00',
                                    'ResponseDescription' => 'Berhasil Mengubah Password'
                                ];
                            } catch (\Throwable $th) {
                                // Return Hasil
                                return $data = [
                                    'ResponseCode' => '99',
                                    'ResponseDescription' => 'Gagal Mengubah Password'
                                ];
                            }
                        } else {
                            // Return Hasil
                            $data = [
                                'ResponseCode' => '99',
                                'ResponseDescription' => 'Password Not Same'
                            ];
                        }
                    } else {
                        $data = [
                            'ResponseCode' => '99',
                            'ResponseDescription' => 'Password old wrong'
                        ];
                    }
                }
            } else {
                $data = [
                    'ResponseCode' => 99,
                    'ResponseDescription' => 'Data tidak ditemukan'
                ];
            }
        }
        // Return response
        return response()->json($data, 200);
    }

}
