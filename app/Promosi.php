<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Promosi extends Model
{
    use HasApiTokens, Notifiable;

    protected $table = 'uemka.promosi';
    protected $fillable = [
        'idpromosi','id_login','judul_promosi','by_promosi','tanggal_promosi','hpyperlink_promosi','image_promosi'
    ];
    protected $primaryKey = 'idpromosi';
}
