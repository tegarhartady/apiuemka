<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kalkulator extends Model
{
    protected $table = 'uemka.kalkulator';
    protected $primaryKey = 'idkalkulator';
    protected $fillable = ['idkalkulator', 'idpartner', 'idproduct', 'jumlahpinjam', 'tenor', 'interestrate', 'jumlahcicil', 'created_at', 'updated_at', 'id_login'];

}
