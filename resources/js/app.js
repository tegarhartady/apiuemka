import './bootstrap'
import Vue from 'vue'
import router from './router'
import App from './layouts/App.vue'
import firebase from 'firebase'
import VueSplide from '@splidejs/vue-splide';

Vue.use( VueSplide );


firebase.initializeApp({
    apiKey: "AIzaSyCMPqOx390IIibfgJ-5BH4jiG112-d8pSg",
    authDomain: "uemka-ce090.firebaseapp.com",
    projectId: "uemka-ce090",
    storageBucket: "uemka-ce090.appspot.com",
    messagingSenderId: "985643452979",
    appId: "1:985643452979:web:62bd4c3977475e56ac3146",
    measurementId: "G-DKNNGGBLNR"
})

import VueGoodTablePlugin from 'vue-good-table';
Vue.use(VueGoodTablePlugin);
import 'vue-good-table/dist/vue-good-table.css'

import Editor from 'v-markdown-editor'
Vue.use(Editor);
new Vue({
    router,
    el: '#app',
    
    render: h => h(App)
})
