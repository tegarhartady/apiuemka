-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: apiuemka
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `idarticle` int(11) NOT NULL,
  `id_login` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `judul_article` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `foto_aticle` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `tanggal_upload` date DEFAULT NULL,
  `by_upload` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `isi_article` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`idarticle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (17,'2014_10_12_000000_create_users_table',1),(18,'2014_10_12_100000_create_password_resets_table',1),(19,'2016_06_01_000001_create_oauth_auth_codes_table',1),(20,'2016_06_01_000002_create_oauth_access_tokens_table',1),(21,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(22,'2016_06_01_000004_create_oauth_clients_table',1),(23,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(24,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('058e728dcf45fb3578d547df51e2b4c8137cea291086b3681d80fcc1b5cf718bb52309f2e56a0173',7,1,'Personal Access Token','[]',0,'2020-12-04 02:34:07','2020-12-04 02:34:07','2021-12-04 09:34:07'),('17827437b65663d7428b2cefccd228d5496697e64aad505f8268d7608b054a58c453cfcc270ac6ba',7,1,'Personal Access Token','[]',1,'2020-12-07 19:30:55','2020-12-07 19:30:55','2021-12-08 02:30:55'),('1b41b747b33575cc501c219bca06b623eba7cbd6f1691affc00862d80a15b7d4e095c79d4d4861d0',1,1,'Personal Access Token','[]',0,'2020-12-03 23:33:23','2020-12-03 23:33:23','2021-12-04 06:33:23'),('2d0ef3754a5572489c488838d1ec4c1028c10cf35bc01dab35906257e5ccba36329112f40e7b4af9',2,1,'Personal Access Token','[]',1,'2020-12-04 00:20:33','2020-12-04 00:20:33','2021-12-04 07:20:33'),('305f5ebbb28ce246fc4a3053bc533951592b31ebcec4ed50c8aaab7fe5ac0d2a1a70ef4c54de4e16',7,1,'Personal Access Token','[]',1,'2020-12-07 01:34:26','2020-12-07 01:34:26','2021-12-07 08:34:26'),('4c2152b0307fb798b94531b15dd114e0c69c7425baffa25dc1663195caee936184e82ae801382e79',6,1,'Personal Access Token','[]',0,'2020-12-04 02:31:22','2020-12-04 02:31:22','2021-12-04 09:31:22'),('c59964f4d81eadbdbadbbd2330c5d85e784780885ef55d7c854efe031528af1cbac377f618103133',7,1,'Personal Access Token','[]',1,'2020-12-06 20:01:43','2020-12-06 20:01:43','2021-12-07 03:01:43'),('e2f35ece423f222750e38a0329aed8f64b672e2a6020f00231b5de5ee6a916de240c3e9612e90c1e',7,1,'Personal Access Token','[]',1,'2020-12-07 01:18:41','2020-12-07 01:18:41','2021-12-07 08:18:41');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','qJq3WlhYtHKgZuW1SgldEwWguoEUdsFTntHPxwCo',NULL,'http://localhost',1,0,0,'2020-12-03 23:23:34','2020-12-03 23:23:34'),(2,NULL,'Laravel Password Grant Client','yxAdeXI1S1D0dwinC6Ylj6YhacvI5VCGJ0oxEhhM','users','http://localhost',0,1,0,'2020-12-03 23:23:34','2020-12-03 23:23:34');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-12-03 23:23:34','2020-12-03 23:23:34');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `idpartner` int(11) NOT NULL AUTO_INCREMENT,
  `nama_partner` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `alamat_partner` longtext COLLATE latin1_bin DEFAULT NULL,
  `nohp_partner` varchar(18) COLLATE latin1_bin DEFAULT NULL,
  `website_partner` varchar(105) COLLATE latin1_bin DEFAULT NULL,
  `jenis_partner` varchar(45) COLLATE latin1_bin DEFAULT NULL,
  `desc_partner` longtext COLLATE latin1_bin DEFAULT NULL,
  `visimisi_partner` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `id_login` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idpartner`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-07 08:26:42','2020-12-08 03:49:54'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-07 09:53:08','2020-12-08 02:29:27'),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-08 02:07:51','2020-12-08 02:29:19'),(4,'Susilo sada','Jakartasss','081236123471','www.asdf.com','Koperasi','parungpanjang','mengkedepankan gaya hidup dengan IT',NULL,'2020-12-08 02:07:58','2020-12-08 02:07:58'),(5,'Masaki suda','Jakartasss','123123123123','www.asdf.com','Koperasi','parungpanjang','mengkedepankan gaya hidup dengan IT',NULL,'2020-12-08 02:29:46','2020-12-08 02:29:46');
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL,
  `idpartner` int(11) DEFAULT NULL,
  `namaperusahaan` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `kode_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `nama_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `desc_product` longtext COLLATE latin1_bin DEFAULT NULL,
  `persyaratan_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `pic_email` varchar(155) COLLATE latin1_bin DEFAULT NULL,
  `nohp_pic` varchar(18) COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`idproduct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_lembaga`
--

DROP TABLE IF EXISTS `profile_lembaga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_lembaga` (
  `id_perusahaan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(45) NOT NULL,
  `email_perusahaan` varchar(255) NOT NULL,
  `password_perusahaan` varchar(255) NOT NULL,
  `alamat_perusahaan` varchar(255) DEFAULT NULL,
  `website_perusahaan` varchar(255) DEFAULT NULL,
  `nama_lengkap_pic` varchar(255) DEFAULT NULL,
  `nomor_handphone_pic` varchar(20) DEFAULT NULL,
  `alamat_pic` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_perusahaan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_lembaga`
--

LOCK TABLES `profile_lembaga` WRITE;
/*!40000 ALTER TABLE `profile_lembaga` DISABLE KEYS */;
INSERT INTO `profile_lembaga` VALUES (1,'Instagram Univercity','lili@fb.com','$2y$10$11zwodVl4YHmr8BkXLp6h.NpeXEAO5Q6Tk1FxmpFS3AbIIarZxnXe',NULL,'Instagram.com','Tegar Hartady',NULL,'parungpanjang','2020-12-04 00:14:10','2020-12-04 00:14:09'),(2,'Instagram Univercity','garr@fb.com','$2y$10$l50Fvmxzdl/ksB.LKmn58u3mTcpADHB438yFf3TjucWhw8I/ghwkC',NULL,'Instagram.com','Tegar Hartady',NULL,'parungpanjang','2020-12-04 00:22:30','2020-12-04 00:22:30'),(3,'Instagram Univercity','pair@mail.com','$2y$10$POHVdk01WTFPTCpF1KOf9eliWBY5si/qR7OYAENtuxyqqwZ.FaYp.',NULL,'Instagram.com','Tegar Hartady',NULL,'parungpanjang','2020-12-04 00:56:56','2020-12-04 00:56:56'),(4,'Instagram Univercity','andi@mail.com','$2y$10$FYrT4glhTr9iYO7tjHj5LOwsFH2Rj.4e8ddF/h55voDKLonZKELpm',NULL,'Instagram.com','Tegar Hartady',NULL,'parungpanjang','2020-12-04 02:26:48','2020-12-04 02:26:48'),(5,'Instagram Univercity','hartady@mail.com','$2y$10$1mLpQdjYccpMdDCaEd2U4OTTnshutJ6MSXAwkJVW74LDeHCTkdad2',NULL,'Instagram.com','Tegar Hartady',NULL,'parungpanjang','2020-12-04 02:33:40','2020-12-04 02:33:40');
/*!40000 ALTER TABLE `profile_lembaga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promosi`
--

DROP TABLE IF EXISTS `promosi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promosi` (
  `idpromosi` int(11) NOT NULL,
  `id_login` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `judul_promosi` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `by_promosi` varchar(155) COLLATE latin1_bin DEFAULT NULL,
  `tanggal_promosi` datetime DEFAULT NULL,
  `hyperlink_promosi` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`idpromosi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promosi`
--

LOCK TABLES `promosi` WRITE;
/*!40000 ALTER TABLE `promosi` DISABLE KEYS */;
/*!40000 ALTER TABLE `promosi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sosmed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Instagram Univercity','lili@fb.com',NULL,'$2y$10$Ef.Zveq1ptTGcbFs46lKveJNVFNWzITX/2sAL3WsK3wp/Rg68QAIC',NULL,'Instagram.com',3,NULL,'2020-12-04 00:14:10','2020-12-04 00:14:10',NULL),(2,'takikun1','sisi@mail.com',NULL,'$2y$10$wB1ZI/WZ/uFdTIUmcZ0H3ONmfOcZStuHOd/5Go/XteZpguqT1/hWa','087652431231','Tegar Hartady',2,NULL,'2020-12-04 00:20:18','2020-12-04 00:20:18',NULL),(3,'Instagram Univercity','garr@fb.com',NULL,'$2y$10$r9sTo2U1kR1iWViXvWeMCe0T5DFEHqHO5TWgAu6CPekexHgvfXWuq',NULL,'Instagram.com',3,NULL,'2020-12-04 00:22:30','2020-12-04 00:22:30',NULL),(4,'kuku','kuku@mail.com',NULL,'$2y$10$nRuawg4sD4M9ea3YRIPeA.OY99a5.QggH1fWSsXK6dn9y2Ym5r8lS','087652431231','Tegar Hartady',2,NULL,'2020-12-04 00:55:12','2020-12-04 00:55:12',NULL),(5,'Instagram Univercity','pair@mail.com',NULL,'$2y$10$92Rff.qwKym6u3Waij3NxuXMzXl9rY0ThKh9o.R3lh6jg.IW9/bIm',NULL,'Instagram.com',3,NULL,'2020-12-04 00:56:56','2020-12-04 00:56:56',NULL),(6,'Instagram Univercity','andi@mail.com',NULL,'$2y$10$FYrT4glhTr9iYO7tjHj5LOwsFH2Rj.4e8ddF/h55voDKLonZKELpm','087652431231','Instagram.com',2,NULL,'2020-12-04 02:26:48','2020-12-04 02:26:48',NULL),(7,'Instagram Univercity','hartady@mail.com',NULL,'$2y$10$1mLpQdjYccpMdDCaEd2U4OTTnshutJ6MSXAwkJVW74LDeHCTkdad2',NULL,'Instagram.com',3,NULL,'2020-12-04 02:33:40','2020-12-04 02:33:40',NULL),(8,'kuku','baihaki@mail.com',NULL,'$2y$10$0ptlSXaEI/mabrbo5XJU5OAUHMYju1dvIh59rIvIv67W2SGr3Ik0G','087652431231','Tegar Hartady',2,NULL,'2020-12-07 01:37:30','2020-12-07 01:37:30',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-08 11:19:45
